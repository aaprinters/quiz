<?php
/*
 * Plugin Name: WP Quiz
 * Plugin URI:  https://mythemeshop.com/plugins/wp-quiz/
 * Description: WP Quiz makes it incredibly easy to add professional, multimedia quizzes to any website! Fully feature rich and optimized for social sharing. Make your site more interactive!
 * Version:     1.1.5
 * Author:      MyThemeShop
 * Author URI:  https://mythemeshop.com/
 *
 * Text Domain: wp-quiz
 * Domain Path: /languages/
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! class_exists( 'WP_Quiz_Plugin' ) ) :


	/**
	 * Register the plugin.
	 *
	 * Display the administration panel, insert JavaScript etc.
	 */
	class WP_Quiz_Plugin {

		/**
		 * Hold plugin version
		 * @var string
		 */
		public $version = '1.1.5';

		/**
		 * Hold an instance of WP_Quiz_Plugin class.
		 *
		 * @var WP_Quiz_Plugin
		 */
		protected static $instance = null;

		/**
		 * @var WP Quiz
		 */
		public $quiz = null;

		/**
		 * Plugin url.
		 * @var string
		 */
		private $plugin_url = null;

		/**
		 * Plugin path.
		 * @var string
		 */
		private $plugin_dir = null;

		/**
		 * Main WP_Quiz_Plugin instance.
		 * @return WP_Quiz_Plugin - Main instance.
		 */
		public static function get_instance() {

			if ( is_null( self::$instance ) ) {
				self::$instance = new WP_Quiz_Plugin;
			}

			return self::$instance;
		}

		/**
		 * You cannot clone this class.
		 */
		public function __clone() {
			_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'wp-quiz' ), $this->version );
		}

		/**
		 * You cannot unserialize instances of this class.
		 */
		public function __wakeup() {
			_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'wp-quiz' ), $this->version );
		}

		/**
		 * Constructor
		 */
		public function __construct() {
			$this->includes();
			$this->hooks();
			$this->setup_shortcode();
		}

		/**
		 * Load required classes
		 */
		private function includes() {

			//auto loader
			spl_autoload_register( array( $this, 'autoloader' ) );

			new WP_Quiz_Admin;
		}

		/**
		 * Autoload classes
		 */
		public function autoloader( $class ) {

			$dir             = $this->plugin_dir() . 'inc' . DIRECTORY_SEPARATOR;
			$class_file_name = 'class-' . str_replace( array( 'wp_quiz_', '_' ), array( '', '-' ), strtolower( $class ) ) . '.php';

			if ( file_exists( $dir . $class_file_name ) ) {
				require $dir . $class_file_name;
			}
		}

		/**
		 * Register the [wp_quiz] shortcode.
		 */
		private function setup_shortcode() {
			add_shortcode( 'wp_quiz', array( $this, 'register_shortcode' ) );
		}

		/**
		 * Hook WP Quiz into WordPress
		 */
		private function hooks() {
			

			// Common
			add_action( 'init', array( $this, 'load_plugin_textdomain' ) );
			add_action( 'init', array( $this, 'register_post_type' ) );

			// Frontend
			add_action( 'wp_head', array( $this, 'inline_script' ), 1 );
			add_filter( 'the_content', array( $this, 'create_quiz_page' ) );

			// Ajax
			add_action( 'wp_ajax_check_image_file', array( $this, 'check_image_file' ) );
			add_action( 'wp_ajax_check_video_file', array( $this, 'check_video_file' ) );

			add_action( 'wp_ajax_wpquiz_get_debug_log', array( $this, 'get_debug_log' ) );

			// FB SDK version 2.9 fix
			if ( isset( $_GET['fbs'] ) && ! empty( $_GET['fbs'] ) ) {
				add_action( 'template_redirect', array( $this, 'fb_share_fix' ) );
			}
		}

		/**
		 * Initialise translations
		 */
		public function load_plugin_textdomain() {
			load_plugin_textdomain( 'wp-quiz', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
			add_shortcode( 'quiz_front', array( $this, 'contact_form' ));			
		}
		
		
	//Custom Front End Form.

	function contact_form( $atts ){
			
					if(isset($_POST["submit_quiz"])){
							$my_quiz = array(
								'post_title'    => 'My Quiz',
								'post_content'  => 'This is my Quiz.',
								'post_status'   => 'publish',
								'post_type'   => 'wp_quiz'
							);
							$new_quiz_type = 'trivia';
							// Insert the post into the database.
							$quiz_id = wp_insert_post( $my_quiz);
							update_post_meta( $quiz_id, 'quiz_type', $new_quiz_type );
							$important_to = $_POST['important_to'];
							$your_wish = $_POST['your_wish'];
							$most_happy = $_POST['most_happy'];
							$won_a_lottery = $_POST['won_a_lottery'];
							$movies_like = $_POST['movies_like'];
							$favourite_flavour = $_POST['favourite_flavour'];
							$afraid_of = $_POST['afraid_of'];
							$intelligent = $_POST['intelligent'];
							$you_choose = $_POST['you_choose'];
							$cloth_colour = $_POST['cloth_colour'];
							$perfect_evening = $_POST['perfect_evening'];
							$smart_phone = $_POST['smart_phone'];
							$your_soulmate = $_POST['your_soulmate'];
							$most_drink = $_POST['most_drink'];
							$favourite_food = $_POST['favourite_food'];
							$fast_food = $_POST['fast_food'];
							$dream_to_pet = $_POST['dream_to_pet'];
							$freetime = $_POST['freetime'];
							$your_choice = $_POST['your_choice'];
							$listen_songs = $_POST['listen_songs'];
							
												
							
							$you_choose = $_POST['you_choose'];
								$answer1 = array(
											0 => array(
												'title' => 'Love',
												'image' => '',
												'isCorrect' => '0'
											),
											1 => array(
												'title' => 'Status',
												'image' => '',
												'isCorrect' => '1'
											),
											2 => array(
												'title' => 'Money',
												'image' => '',
												'isCorrect' => '0'
											),
											3 => array(
												'title' => 'Power',
												'image' => '',
												'isCorrect' => '0'
											),
								);
								$new_answer1=array();
								$answer_arr1=array();
								foreach($answer1 as $answer_arr_key=>$answer_arr1){
									foreach($answer_arr1 as $answer_key=>$answer_value){
										$answer_title = $answer_arr1['title'];
										//$answer_title = $answer_title;
										if($important_to == $answer_title){
											$answer_arr1['isCorrect'] = '1';
										}else{
											$answer_arr1['isCorrect'] = '0';
										}
										
									}
									$new_answer1[$answer_arr_key]=$answer_arr1;	
								}
								$answer2 = array(
											0 => array(
												'title' => '1 Million Dollar',
												'image' => '',
												'isCorrect' => '0'
											),
											1 => array(
												'title' => 'Beautiful Wife/Handsome Husband',
												'image' => '',
												'isCorrect' => '1'
											),
											2 => array(
												'title' => 'To be the PM of Country',
												'image' => '',
												'isCorrect' => '0'
											),
											3 => array(
												'title' => '3 More Wishes',
												'image' => '',
												'isCorrect' => '0'
											),
											
								);
								$new_answer2=array();
								$answer_arr2=array();
								foreach($answer2 as $answer_arr_key=>$answer_arr2){
									foreach($answer_arr2 as $answer_key=>$answer_value){
										$answer_title = $answer_arr2['title'];
										if($your_wish == $answer_title){
											$answer_arr2['isCorrect'] = '1';
										}else{
											$answer_arr2['isCorrect'] = '0';
										}
										
									}
									$new_answer2[$answer_arr_key]=$answer_arr2;	
								}
								$answer3 = array(
											0 => array(
												'title' => 'Money',
												'image' => '',
												'isCorrect' => '0'
											),
											1 => array(
												'title' => 'Love',
												'image' => '',
												'isCorrect' => '1'
											),
											2 => array(
												'title' => 'Time with Friends',
												'image' => '',
												'isCorrect' => '0'
											),	
										
								);
								$new_answer3=array();
								$answer_arr3=array();
								foreach($answer3 as $answer_arr_key=>$answer_arr3){
									foreach($answer_arr3 as $answer_key=>$answer_value){
										$answer_title = $answer_arr3['title'];
										$answer_title = $answer_title;
										if($most_happy == $answer_title){
											$answer_arr3['isCorrect'] = '1';
										}else{
											$answer_arr3['isCorrect'] = '0';
										}
										
									}
									$new_answer3[$answer_arr_key]=$answer_arr3;	
								}
								$answer4 = array(
											0 => array(
												'title' => 'Travel the world',
												'image' => '',
												'isCorrect' => '0'
											),
											1 => array(
												'title' => 'Invest it',
												'image' => '',
												'isCorrect' => '1'
											),
											2 => array(
												'title' => 'Eat at fancy restaurants',
												'image' => '',
												'isCorrect' => '0'
											),

											3 => array(
												'title' => 'Buy a sports car',
												'image' => '',
												'isCorrect' => '0'
											),	
										
								);
								$new_answer4=array();
								$answer_arr4=array();
								foreach($answer4 as $answer_arr_key=>$answer_arr4){
									foreach($answer_arr4 as $answer_key=>$answer_value){
										$answer_title = $answer_arr4['title'];
										$answer_title = $answer_title;
										if($won_a_lottery == $answer_title){
											$answer_arr4['isCorrect'] = '1';
										}else{
											$answer_arr4['isCorrect'] = '0';
										}
										
									}
									$new_answer4[$answer_arr_key]=$answer_arr4;	
								}
								$answer20 = array(
											0 => array(
												'title' => 'Romance',
												'image' => '',
												'isCorrect' => '0'
											),
											1 => array(
												'title' => 'Action',
												'image' => '',
												'isCorrect' => '1'
											),
											2 => array(
												'title' => 'Thriller',
												'image' => '',
												'isCorrect' => '0'
											),

											3 => array(
												'title' => 'Comedy',
												'image' => '',
												'isCorrect' => '0'
											),	

											4 => array(
												'title' => 'Horror',
												'image' => '',
												'isCorrect' => '0'
											),
											5 => array(
												'title' => 'Magic',
												'image' => '',
												'isCorrect' => '0'
											),
											6 => array(
												'title' => 'Fantasy',
												'image' => '',
												'isCorrect' => '0'
											),
											7 => array(
												'title' => 'Sci Fi',
												'image' => '',
												'isCorrect' => '0'
											),
											8 => array(
												'title' => 'Animated',
												'image' => '',
												'isCorrect' => '0'
											),										
								);
								$new_answer20=array();
								$answer_arr20=array();
								foreach($answer20 as $answer_arr_key=>$answer_arr20){
									foreach($answer_arr20 as $answer_key=>$answer_value){
										$answer_title = $answer_arr20['title'];
										$answer_title = $answer_title;
										if($movies_like == $answer_title){
											$answer_arr20['isCorrect'] = '1';
										}else{
											$answer_arr20['isCorrect'] = '0';
										}
										
									}
									$new_answer20[$answer_arr_key]=$answer_arr20;	
								}
								$answer5 = array(
											0 => array(
												'title' => 'Vanilla',
												'image' => '',
												'isCorrect' => '0'
											),
											1 => array(
												'title' => 'Butter Scotch',
												'image' => '',
												'isCorrect' => '1'
											),
											2 => array(
												'title' => 'Strawberry',
												'image' => '',
												'isCorrect' => '0'
											),

											3 => array(
												'title' => 'Chocolate',
												'image' => '',
												'isCorrect' => '0'
											),	

											4 => array(
												'title' => 'Black Current',
												'image' => '',
												'isCorrect' => '0'
											),
											5 => array(
												'title' => 'Mango',
												'image' => '',
												'isCorrect' => '0'
											),
											

								);
								$new_answer5=array();
								$answer_arr5=array();
								foreach($answer5 as $answer_arr_key=>$answer_arr5){
									foreach($answer_arr5 as $answer_key=>$answer_value){
										$answer_title = $answer_arr5['title'];
										$answer_title = $answer_title;
										if($favourite_flavour == $answer_title){
											$answer_arr5['isCorrect'] = '1';
										}else{
											$answer_arr5['isCorrect'] = '0';
										}
										
									}
									$new_answer5[$answer_arr_key]=$answer_arr5;	
								}
								$answer6 = array(
											0 => array(
												'title' => 'High Places',
												'image' => '',
												'isCorrect' => '0'
											),
											1 => array(
												'title' => 'Darkness',
												'image' => '',
												'isCorrect' => '1'
											),
											2 => array(
												'title' => 'Insects/Cockroaches/Rats/Flies',
												'image' => '',
												'isCorrect' => '0'
											),

											3 => array(
												'title' => 'Flying',
												'image' => '',
												'isCorrect' => '0'
											),	

											4 => array(
												'title' => 'Injection',
												'image' => '',
												'isCorrect' => '0'
											),
											5 => array(
												'title' => 'Alone at Home',
												'image' => '',
												'isCorrect' => '0'
											),
											6 => array(
												'title' => 'Nothing',
												'image' => '',
												'isCorrect' => '0'
											),

								);
								$new_answer6=array();
								$answer_arr6=array();
								foreach($answer6 as $answer_arr_key=>$answer_arr6){
									foreach($answer_arr6 as $answer_key=>$answer_value){
										$answer_title = $answer_arr6['title'];
										$answer_title = $answer_title;
										if($afraid_of == $answer_title){
											$answer_arr6['isCorrect'] = '1';
										}else{
											$answer_arr6['isCorrect'] = '0';
										}
										
									}
									$new_answer6[$answer_arr_key]=$answer_arr6;	
								}
								$answer7 = array(
											0 => array(
												'title' => 'Fly like Superman',
												'image' => '',
												'isCorrect' => '0'
											),
											1 => array(
												'title' => 'Be invisible',
												'image' => '',
												'isCorrect' => '1'
											),
											2 => array(
												'title' => 'See Ghosts',
												'image' => '',
												'isCorrect' => '0'
											),

											3 => array(
												'title' => 'Read Minds of the people',
												'image' => '',
												'isCorrect' => '0'
											),	

											4 => array(
												'title' => 'Post navigation',
												'image' => '',
												'isCorrect' => '0'
											),
											

								);
								$new_answer7=array();
								$answer_arr7=array();
								foreach($answer7 as $answer_arr_key=>$answer_arr7){
									foreach($answer_arr7 as $answer_key=>$answer_value){
										$answer_title = $answer_arr7['title'];
										if($you_choose == $answer_title){
											$answer_arr7['isCorrect'] = '1';
										}else{
											$answer_arr7['isCorrect'] = '0';
										}
										
									}
									$new_answer7[$answer_arr_key]=$answer_arr7;	
								}

								$answer8 = array(
											0 => array(
												'title' => 'Red',
												'image' => '',
												'isCorrect' => '0'
											),
											1 => array(
												'title' => 'Blue',
												'image' => '',
												'isCorrect' => '1'
											),
											2 => array(
												'title' => 'Orange',
												'image' => '',
												'isCorrect' => '0'
											),

											3 => array(
												'title' => 'Pink',
												'image' => '',
												'isCorrect' => '0'
											),	

											4 => array(
												'title' => 'Yellow',
												'image' => '',
												'isCorrect' => '0'
											),
											5 => array(
												'title' => 'Black',
												'image' => '',
												'isCorrect' => '0'
											),
											6 => array(
												'title' => 'Green',
												'image' => '',
												'isCorrect' => '0'
											),
											7 => array(
												'title' => 'White',
												'image' => '',
												'isCorrect' => '0'
											),
																				
								);
								$new_answer8=array();
								$answer_arr8=array();
								foreach($answer8 as $answer_arr_key=>$answer_arr8){
									foreach($answer_arr8 as $answer_key=>$answer_value){
										$answer_title = $answer_arr8['title'];

										if($cloth_colour == $answer_title){
											$answer_arr8['isCorrect'] = '1';
										}else{
											$answer_arr8['isCorrect'] = '0';
										}
										
									}
									$new_answer8[$answer_arr_key]=$answer_arr8;	
								}

								$answer9 = array(
											0 => array(
												'title' => 'Candle Light Dinner',
												'image' => '',
												'isCorrect' => '0'
											),
											1 => array(
												'title' => 'Movie with Friends',
												'image' => '',
												'isCorrect' => '1'
											),
											2 => array(
												'title' => 'Hit a Club',
												'image' => '',
												'isCorrect' => '0'
											),

											3 => array(
												'title' => 'Cricket Match',
												'image' => '',
												'isCorrect' => '0'
											),	

											4 => array(
												'title' => 'Football Match',
												'image' => '',
												'isCorrect' => '0'
											),
											5 => array(
												'title' => 'Travel',
												'image' => '',
												'isCorrect' => '0'
											),
											6 => array(
												'title' => 'Be Alone',
												'image' => '',
												'isCorrect' => '0'
											),
											7 => array(
												'title' => 'Sleeping',
												'image' => '',
												'isCorrect' => '0'
											),
																				
								);
								$new_answer9=array();
								$answer_arr9=array();
								foreach($answer9 as $answer_arr_key=>$answer_arr9){
									foreach($answer_arr9 as $answer_key=>$answer_value){
										$answer_title = $answer_arr9['title'];
										if($perfect_evening == $answer_title){
											$answer_arr9['isCorrect'] = '1';
										}else{
											$answer_arr9['isCorrect'] = '0';
										}
										
									}
									$new_answer9[$answer_arr_key]=$answer_arr9;	
								}

								$answer10 = array(
											0 => array(
												'title' => 'Apple Iphone',
												'image' => '',
												'isCorrect' => '0'
											),
											1 => array(
												'title' => 'Android Smartphone',
												'image' => '',
												'isCorrect' => '1'
											),
																					
								);
																$new_answer10=array();
								$answer_arr10=array();
								foreach($answer10 as $answer_arr_key=>$answer_arr10){
									foreach($answer_arr10 as $answer_key=>$answer_value){
										$answer_title = $answer_arr10['title'];
										if($smart_phone == $answer_title){
											$answer_arr10['isCorrect'] = '1';
										}else{
											$answer_arr10['isCorrect'] = '0';
										}
										
									}
									$new_answer10[$answer_arr_key]=$answer_arr10;	
								}

								$answer11 = array(
											0 => array(
												'title' => 'Hot/Smart',
												'image' => '',
												'isCorrect' => '0'
											),
											1 => array(
												'title' => 'Intelligent',
												'image' => '',
												'isCorrect' => '1'
											),
																					
								);
							
								$new_answer11=array();
								$answer_arr11=array();
								foreach($answer11 as $answer_arr_key=>$answer_arr11){
									foreach($answer_arr11 as $answer_key=>$answer_value){
										$answer_title = $answer_arr11['title'];
										if($intelligent == $answer_title){
											$answer_arr11['isCorrect'] = '1';
										}else{
											$answer_arr11['isCorrect'] = '0';
										}
										
									}
									$new_answer11[$answer_arr_key]=$answer_arr11;	
								}

								$answer12 = array(
											0 => array(
												'title' => 'Paris',
												'image' => '',
												'isCorrect' => '0'
											),
											1 => array(
												'title' => 'Bali',
												'image' => '',
												'isCorrect' => '1'
											),
											2 => array(
												'title' => 'Sonoma',
												'image' => '',
												'isCorrect' => '0'
											),

											3 => array(
												'title' => 'Venice',
												'image' => '',
												'isCorrect' => '0'
											),	

											4 => array(
												'title' => 'Bahamas',
												'image' => '',
												'isCorrect' => '0'
											),
											5 => array(
												'title' => 'Cape Town',
												'image' => '',
												'isCorrect' => '0'
											),
											6 => array(
												'title' => 'Thailand',
												'image' => '',
												'isCorrect' => '0'
											),
											7 => array(
												'title' => 'Greece',
												'image' => '',
												'isCorrect' => '0'
											),
											8 => array(
												'title' => 'Maldives',
												'image' => '',
												'isCorrect' => '0'
											),
											9 => array(
												'title' => 'Mauritus',
												'image' => '',
												'isCorrect' => '0'
											),
																				
								);
								$new_answer12=array();
								$answer_arr12=array();
								foreach($answer12 as $answer_arr_key=>$answer_arr12){
									foreach($answer_arr12 as $answer_key=>$answer_value){
										$answer_title = $answer_arr12['title'];
										if($your_soulmate == $answer_title){
											$answer_arr12['isCorrect'] = '1';
										}else{
											$answer_arr12['isCorrect'] = '0';
										}
										
									}
									$new_answer12[$answer_arr_key]=$answer_arr12;	
								}

								$answer13 = array(
											0 => array(
												'title' => 'Tea',
												'image' => '',
												'isCorrect' => '0'
											),
											1 => array(
												'title' => 'Coffee',
												'image' => '',
												'isCorrect' => '1'
											),
											2 => array(
												'title' => 'Milk',
												'image' => '',
												'isCorrect' => '0'
											),

											3 => array(
												'title' => 'Alcohol',
												'image' => '',
												'isCorrect' => '0'
											),	

											4 => array(
												'title' => 'Green Tea',
												'image' => '',
												'isCorrect' => '0'
											),
											5 => array(
												'title' => 'Water',
												'image' => '',
												'isCorrect' => '0'
											),
											6 => array(
												'title' => 'Juice',
												'image' => '',
												'isCorrect' => '0'
											),
																				
								);
								$new_answer13=array();
								$answer_arr13=array();
								foreach($answer13 as $answer_arr_key=>$answer_arr13){
									foreach($answer_arr13 as $answer_key=>$answer_value){
										$answer_title = $answer_arr13['title'];

										if($most_drink == $answer_title){
											$answer_arr13['isCorrect'] = '1';
										}else{
											$answer_arr13['isCorrect'] = '0';
										}
										
									}
									$new_answer13[$answer_arr_key]=$answer_arr13;	
								}

								$answer14 = array(
											0 => array(
												'title' => 'Pizza',
												'image' => '',
												'isCorrect' => '0'
											),
											1 => array(
												'title' => 'Burger',
												'image' => '',
												'isCorrect' => '1'
											),
											2 => array(
												'title' => 'Noodles',
												'image' => '',
												'isCorrect' => '0'
											),

											3 => array(
												'title' => 'Salad',
												'image' => '',
												'isCorrect' => '0'
											),	

											4 => array(
												'title' => 'Pasta',
												'image' => '',
												'isCorrect' => '0'
											),
											5 => array(
												'title' => 'Simple Food',
												'image' => '',
												'isCorrect' => '0'
											),

																				
								);
								$new_answer14=array();
								$answer_arr14=array();
								foreach($answer14 as $answer_arr_key=>$answer_arr14){
									foreach($answer_arr14 as $answer_key=>$answer_value){
										$answer_title = $answer_arr14['title'];
										if($favourite_food == $answer_title){
											$answer_arr14['isCorrect'] = '1';
										}else{
											$answer_arr14['isCorrect'] = '0';
										}
										
									}
									$new_answer14[$answer_arr_key]=$answer_arr14;	
								}

								$answer15 = array(
											0 => array(
												'title' => 'Pizza Hut',
												'image' => '',
												'isCorrect' => '0'
											),
											1 => array(
												'title' => 'Dominos',
												'image' => '',
												'isCorrect' => '1'
											),
											2 => array(
												'title' => 'Subway',
												'image' => '',
												'isCorrect' => '0'
											),

											3 => array(
												'title' => 'McDonalds',
												'image' => '',
												'isCorrect' => '0'
											),	

											4 => array(
												'title' => 'KFC',
												'image' => '',
												'isCorrect' => '0'
											),
											5 => array(
												'title' => 'Burger King',
												'image' => '',
												'isCorrect' => '0'
											),
											6 => array(
												'title' => 'Wendys',
												'image' => '',
												'isCorrect' => '0'
											),
											7 => array(
												'title' => 'Taco Bell',
												'image' => '',
												'isCorrect' => '0'
											),
											8 => array(
												'title' => 'Maldives',
												'image' => '',
												'isCorrect' => '0'
											),
											9 => array(
												'title' => 'Baskin Robbins',
												'image' => '',
												'isCorrect' => '0'
											),
																				
								);
								$new_answer15=array();
								$answer_arr15=array();
								foreach($answer15 as $answer_arr_key=>$answer_arr15){
									foreach($answer_arr15 as $answer_key=>$answer_value){
										$answer_title = $answer_arr15['title'];
										if($fast_food == $answer_title){
											$answer_arr15['isCorrect'] = '1';
										}else{
											$answer_arr15['isCorrect'] = '0';
										}
										
									}
									$new_answer15[$answer_arr_key]=$answer_arr15;	
								}

								$answer16 = array(
											0 => array(
												'title' => 'A Dog',
												'image' => '',
												'isCorrect' => '0'
											),
											1 => array(
												'title' => 'Love Birds',
												'image' => '',
												'isCorrect' => '1'
											),
											2 => array(
												'title' => 'A Rabbit',
												'image' => '',
												'isCorrect' => '0'
											),

											3 => array(
												'title' => 'A Cat',
												'image' => '',
												'isCorrect' => '0'
											),	

											4 => array(
												'title' => 'A Lion',
												'image' => '',
												'isCorrect' => '0'
											),
										
																				
								);
								$new_answer16=array();
								$answer_arr16=array();
								foreach($answer16 as $answer_arr_key=>$answer_arr16){
									foreach($answer_arr16 as $answer_key=>$answer_value){
										$answer_title = $answer_arr16['title'];
										if($dream_to_pet == $answer_title){
											$answer_arr16['isCorrect'] = '1';
										}else{
											$answer_arr16['isCorrect'] = '0';
										}
										
									}
									$new_answer16[$answer_arr_key]=$answer_arr16;	
								}

								$answer17 = array(
											0 => array(
												'title' => 'Gym',
												'image' => '',
												'isCorrect' => '0'
											),
											1 => array(
												'title' => 'Sleeping',
												'image' => '',
												'isCorrect' => '1'
											),
											2 => array(
												'title' => 'Shopping',
												'image' => '',
												'isCorrect' => '0'
											),

											3 => array(
												'title' => 'Swimming',
												'image' => '',
												'isCorrect' => '0'
											),	

											4 => array(
												'title' => 'Reading',
												'image' => '',
												'isCorrect' => '0'
											),
											5 => array(
												'title' => 'Travel',
												'image' => '',
												'isCorrect' => '0'
											),
											6 => array(
												'title' => 'Movie',
												'image' => '',
												'isCorrect' => '0'
											),
																				
								);
								$new_answer17=array();
								$answer_arr17=array();
								foreach($answer17 as $answer_arr_key=>$answer_arr17){
									foreach($answer_arr17 as $answer_key=>$answer_value){
										$answer_title = $answer_arr17['title'];
										if($freetime == $answer_title){
											$answer_arr17['isCorrect'] = '1';
										}else{
											$answer_arr17['isCorrect'] = '0';
										}
										
									}
									$new_answer17[$answer_arr_key]=$answer_arr17;	
								}

								$answer18 = array(
											0 => array(
												'title' => 'Change the Past',
												'image' => '',
												'isCorrect' => '0'
											),
											1 => array(
												'title' => 'See the Future',
												'image' => '',
												'isCorrect' => '1'
											),
																					
								);
																$new_answer18=array();
								$answer_arr18=array();
								foreach($answer18 as $answer_arr_key=>$answer_arr18){
									foreach($answer_arr18 as $answer_key=>$answer_value){
										$answer_title = $answer_arr18['title'];
										if($your_choice == $answer_title){
											$answer_arr18['isCorrect'] = '1';
										}else{
											$answer_arr18['isCorrect'] = '0';
										}
										
									}
									$new_answer18[$answer_arr_key]=$answer_arr18;	
								}

								$answer19 = array(
											0 => array(
												'title' => 'Romantic Songs',
												'image' => '',
												'isCorrect' => '0'
											),
											1 => array(
												'title' => 'Party Songs',
												'image' => '',
												'isCorrect' => '1'
											),
											2 => array(
												'title' => 'Pop',
												'image' => '',
												'isCorrect' => '0'
											),

											3 => array(
												'title' => 'Sufi',
												'image' => '',
												'isCorrect' => '0'
											),	

											4 => array(
												'title' => 'Devotional',
												'image' => '',
												'isCorrect' => '0'
											),
											5 => array(
												'title' => 'Classical',
												'image' => '',
												'isCorrect' => '0'
											),
											6 => array(
												'title' => 'Movie',
												'image' => '',
												'isCorrect' => '0'
											),
																				
								);
								$new_answer19=array();
								$answer_arr19=array();
								foreach($answer19 as $answer_arr_key=>$answer_arr19){
									foreach($answer_arr19 as $answer_key=>$answer_value){
										$answer_title = $answer_arr19['title'];
										if($listen_songs == $answer_title){
											$answer_arr19['isCorrect'] = '1';
										}else{
											$answer_arr19['isCorrect'] = '0';
										}
										
									}
									$new_answer19[$answer_arr_key]=$answer_arr19;	
								}

							$questions = array(
											0 => array(
												'title' => 'What is more important to you?',
												'mediaType' => '',
												'answers' => $new_answer1
											),
											1 => array(
												'title' => 'If you meet a genie, what would be your wish?',
												'mediaType' => '',
												'answers' => $new_answer2
											),
											2 => array(
												'title' => 'What would make you most happy?',
												'mediaType' => '',
												'answers' => $new_answer3
											),
											3 => array(
												'title' => 'What would you do if you won a lottery?',
												'mediaType' => '',
												'answers' => $new_answer4
											),
											4 => array(
												'title' => 'What type of movies do you like?',
												'mediaType' => '',
												'answers' => $new_answer20
											),
											5 => array(
												'title' => 'Your favorite flavor is?',
												'mediaType' => '',
												'answers' => $new_answer5
											),
											6 => array(
												'title' => 'What you are afraid of?',
												'mediaType' => '',
												'answers' => $new_answer6
											),
											7 => array(
												'title' => 'What would you choose?',
												'mediaType' => '',
												'answers' => $new_answer7
											),
											8 => array(
												'title' => 'Which colour clothes you wear the most?',
												'mediaType' => '',
												'answers' => $new_answer8
											),
											9 => array(
												'title' => 'What is your idea of a perfect evening?',
												'mediaType' => '',
												'answers' => $new_answer9
											),
											10 => array(
												'title' => 'Do you like Apple Iphone or an Android Smartphone?',
												'mediaType' => '',
												'answers' => $new_answer10
											),

											11 => array(
												'title' => 'Do you want your Wife/Husband to be the Hot/Smart or the Intelligent?',
												'mediaType' => '',
												'answers' => $new_answer11
											),
											12 => array(
												'title' => 'Where would you like to go with your soulmate?',
												'mediaType' => '',
												'answers' => $new_answer12
											),
											13 => array(
												'title' => 'What you drink the most?',
												'mediaType' => '',
												'answers' => $new_answer13
											),
											14 => array(
												'title' => 'What is your favourite food?',
												'mediaType' => '',
												'answers' => $new_answer14
											),
											15 => array(
												'title' => 'Which fast food restaurant do you prefer the most?',
												'mediaType' => '',
												'answers' => $new_answer15
											),
											16 => array(
												'title' => 'Which animal do you dream to pet?',
												'mediaType' => '',
												'answers' => $new_answer16
											),
											17 => array(
												'title' => 'In your freetime, where would you go?',
												'mediaType' => '',
												'answers' => $new_answer17
											),
											18 => array(
												'title' => 'What will you choose?',
												'mediaType' => '',
												'answers' => $new_answer18
											),
											19 => array(
												'title' => 'Which songs you like to listen to?',
												'mediaType' => '',
												'answers' => $new_answer19
											),
											

									);
	 update_post_meta( $quiz_id, 'questions', $questions );
									
	 $quiz_settings = array(
						'question_layout' => 'multiple',
						'skin' => 'traditional',
						'bar_color' => '#00c479',
						'font_color' => '#444',
						'background_color' => 'background_color',
						'animation_in' => 'fade',
						'animation_out' => 'fade',
						'bar_color' => '#00c479',
						'size' => '#444',
						'custom_height' => 'background_color',
						'custom_width' => 'background_color',
						'custom_height' => 'background_color',
						'share_buttons' => array(
												0 => 'fb',
												1 => 'tw',
												2 => 'g+'
											),
						'auto_scroll' => '1',
						'rand_questions' => '0',
						'rand_answers' => '0',
						'restart_questions' => '0',
						'embed_toggle' => '0',
						'show_ads' => '0',
						'show_countdown' => '0',
						'timer' => '0',
						'repeat_ads' => '0',
						
						);
	update_post_meta( $quiz_id, 'settings',  $quiz_settings );
/* Array
(
[0] => Array
	(
		[title] => 0 match
		[image] => 
		[min] => 0
		[max] => 0
		[desc] => 0 match
	)
	) */
							$results = array(
											0 => array(
												'title' => '0 match',
												'image' => '',
												'min' => '0',
												'max' => '0',
												'desc' => '0'
											),
											1 => array(
												'title' => '1 match',
												'image' => '',
												'min' => '1',
												'max' => '1',
												'desc' => '0'
											),
											2 => array(
												'title' => '2 match',
												'image' => '',
												'min' => '2',
												'max' => '2',
												'desc' => '0'
											),
											3 => array(
												'title' => '3 match',
												'image' => '',
												'min' => '3',
												'max' => '3',
												'desc' => '0'
											),
											4 => array(
												'title' => '4 match',
												'image' => '',
												'min' => '4',
												'max' => '4',
												'desc' => '0'
											),
											5 => array(
												'title' => '5 match',
												'image' => '',
												'min' => '5',
												'max' => '5',
												'desc' => '0'
											),
											6 => array(
												'title' => '6 match',
												'image' => '',
												'min' => '6',
												'max' => '6',
												'desc' => '0'
											),
											7 => array(
												'title' => '7 match',
												'image' => '',
												'min' => '7',
												'max' => '7',
												'desc' => '0'
											),
											8 => array(
												'title' => '8 match',
												'image' => '',
												'min' => '8',
												'max' => '8',
												'desc' => '0'
											),
											9 => array(
												'title' => '9 match',
												'image' => '',
												'min' => '9',
												'max' => '9',
												'desc' => '0'
											),
											10 => array(
												'title' => '10 match',
												'image' => '',
												'min' => '10',
												'max' => '10',
												'desc' => '10'
											),
											11 => array(
												'title' => '11 match',
												'image' => '',
												'min' => '11',
												'max' => '11',
												'desc' => '0'
											),
											12 => array(
												'title' => '12 match',
												'image' => '',
												'min' => '12',
												'max' => '12',
												'desc' => '0'
											),
											13 => array(
												'title' => '13 match',
												'image' => '',
												'min' => '13',
												'max' => '13',
												'desc' => '0'
											),
											14 => array(
												'title' => '14 match',
												'image' => '1',
												'min' => '14',
												'max' => '14',
												'desc' => '0'
											),
											15 => array(
												'title' => '15 match',
												'image' => '',
												'min' => '5',
												'max' => '5',
												'desc' => '0'
											),
											16 => array(
												'title' => '16 match',
												'image' => '',
												'min' => '16',
												'max' => '16',
												'desc' => '0'
											),
											17 => array(
												'title' => '17 match',
												'image' => '',
												'min' => '7',
												'max' => '7',
												'desc' => '0'
											),
											18 => array(
												'title' => '18 match',
												'image' => '',
												'min' => '18',
												'max' => '18',
												'desc' => '0'
											),
											19 => array(
												'title' => '19 match',
												'image' => '',
												'min' => '19',
												'max' => '19',
												'desc' => '0'
											),
											20 => array(
												'title' => '20 match',
												'image' => '',
												'min' => '20',
												'max' => '20',
												'desc' => '0'
											)
											

								);
							update_post_meta( $quiz_id, 'results', $results );
							$shareable_link = get_permalink($quiz_id); 
						echo '<h2>Share this link with your friends...</h2>
						<input class="form-control" type="text" value="'.$shareable_link.'">
						<style>
						.quiz_container{
							display:none;
						}
						<style>';
						
					}				
ob_start(); 
$quiz_url = plugins_url('',__FILE__);

?>
<link href="<?php echo $quiz_url.'/assets/css/multi_step.css'; ?>" rel="stylesheet" type="text/css">
<script src="<?php echo $quiz_url.'/assets/js/multi-jquery.js'; ?>" type="text/javascript"></script>
<script src="<?php echo $quiz_url.'/assets/js/work_flow.js'; ?>" type="text/javascript"></script>

<style>
   /* Text selection color */  
   ::-moz-selection { 
   background: black;
   color: white;
   }
   ::selection {
   background: black;
   color: white;
   }
   /* Scrollbar fixings */ 
   body {
   overflow-x: hidden;
   } 
   body::-webkit-scrollbar {
   width: 8px;
   height: 2px;
   border-radius: 0px;
   background-color: #6c33da;
   }
   body::-webkit-scrollbar-thumb {
   border-radius: 0px;
   width: 8px;
   background-color: #422085;
   height: 25px;
   } 
   body::-webkit-scrollbar-corner {
   display: none;
   height: 0px;
   width: 0px;
   }
   /* Overrides for range slider skin */
   .irs {
   height: 40px;
   margin-top: 15px;
   margin-bottom: 30px
   }
   .irs-with-grid {
   height: 85px
   }
   .irs-line {
   height: 12px;
   top: 35px
   }
   .irs-line-left {
   height: 12px;
   background-position: 0 -30px
   }
   .irs-line-mid {
   height: 12px;
   background-position: 0 0
   }
   .irs-line-right {
   height: 12px;
   background-position: 100% -30px
   }
   .irs-bar {
   height: 12px;
   top: 35px;
   background-position: 0 -60px;
   background-color: #6c33da;
   background-image: none;!important
   }
   .irs-bar-edge {
   top: 35px;
   height: 12px;
   width: 15px;
   background-color: #6c33da;
   background-image: none;!important
   -webkit-border-top-left-radius: 4px;
   -webkit-border-bottom-left-radius: 4px;
   border-top-left-radius: 4px;
   border-bottom-left-radius: 4px
   }
   .irs-shadow {
   height: 3px;
   top: 35px;
   background: #000;
   opacity: .25
   }
   .lt-ie9 .irs-shadow {
   filter: alpha(opacity=25)
   }
   .irs-slider {
   width: 25px;
   height: 25px;
   top: 29px;
   background-position: 0 -120px;
   background-image: none;
   background-color: #ccc;
   border-radius: 100%;
   transition: background-color 200ms ease-in
   }
   .irs-slider.state_hover,
   .irs-slider:hover {
   width: 25px;
   height: 25px;
   top: 29px;
   background-position: 0 -120px;
   background-image: none;
   background-color: #6c33da;
   border-radius: 100%
   }
   .irs-min,
   .irs-max {
   color: #999;
   font-size: 0.9em;
   line-height: 1em;
   text-shadow: none;
   top: 0;
   padding: 5px;
   margin-bottom: 25px;
   background: #e1e4e9;
   -moz-border-radius: 4px;
   border-radius: 4px
   }
   .irs-from,
   .irs-to,
   .irs-single {
   color: #fff;
   font-size: 0.9em;
   line-height: 1em;
   text-shadow: none;
   padding: 5px;
   margin-bottom: 25px;
   background: #6c33da;
   -moz-border-radius: 4px;
   border-radius: 4px
   }
   .irs-from:after,
   .irs-to:after,
   .irs-single:after {
   position: absolute;
   display: block;
   content: "";
   bottom: -5px;
   left: 50%;
   width: 0;
   height: 0;
   margin-left: -3px;
   overflow: hidden;
   border: 3px solid transparent;
   border-top-color: #6c33da
   }
   .irs-grid-pol {
   background: #e1e4e9
   }
   .irs-grid-text {
   color: #999
   }
   /* Getting rid of that ugly textarea resize-corner */
   textarea {
   resize: none
   }
   /* textarea scrollbar fixings */
   textarea.area.field.w-input::-webkit-scrollbar {
   width: 14px;
   height: 18px;
   }
   textarea.area.field.w-input::-webkit-scrollbar-thumb {
   height: 6px;
   border: 4px solid rgba(0, 0, 0, 0);
   background-clip: padding-box;
   -webkit-border-radius: 7px;
   background-color: #6c33da;
   -webkit-box-shadow: inset -1px -1px 0px rgba(0, 0, 0, 0.05), inset 1px 1px 0px rgba(0, 0, 0, 0.05);
   }
   textarea.area.field.w-input::-webkit-scrollbar-button {
   width: 0;
   height: 0;
   display: none;
   }
   textarea.area.field.w-input::-webkit-scrollbar-corner {
   background-color: transparent;
   }
      .step.w-slide {
    width: 100%;
    background: no-repeat;
    background: none;
}
.form-content {
    width: 100%;
    margin-bottom: 0;
}
.collaborate-wrap {
    background: no-repeat;
}
label.wq_answerTxtCtr {
    margin: 0;
    border: 1px solid;
    /* border-bottom: none; */
    padding: 8px;
    background: #F2E3DD;
    margin-bottom: 3px;
	font-size: 19px;
    font-weight: lighter;
	font-family: cursive;
}
.wq_questionTextCtr h4 {
    font-size: 22px;
}
.wq_questionTextCtr {
    background: none !important;
}
.step-counter {
    position: relative;
    margin-bottom: 0;
	font-size: 18px;
    font-weight: bold;
}
label.wq_answerTxtCtr input {
    -webkit-appearance: none;
	appearance: none;
}
.content {
    background: none;
}
.collaborate-wrap {
    background: none !important;
}
.w-slider-mask {
    overflow: initial;
}
.next.w-slider-arrow-right {
    position: relative;
    display: block;
    float: right;
}
.previous-button {
    display: none !important;
}
.wq_questionTextCtr h4 {
    font-size: 22px;
}
</style>
 <div class="quiz_container collaborate-wrap">
      <div class="w-container">
            <form method="post" action="">
               <div class="step-paginator w-slider" data-animation="cross" data-disable-swipe="1" data-duration="200" data-hide-arrows="1">
			    <div class="previous w-slider-arrow-left">
                     <div class="previous-button">← Back</div>
                  </div>
                  <div class="next w-slider-arrow-right">
                     <div class="next-button">Next</div>
                  </div>
                  <div class="mask w-slider-mask">
                    <div class="step w-slide">
					<div class="step-counter" data-ix="show-content-onslide">Creating quiz 1/20</div>
                        <div class="collaborate-form-step1">
                            <div class="form-content">
								<div class="wq_singleQuestionCtr">
									<div class="wq_questionTextWrapper quiz-clearfix">
										<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
											<h4>What is more important to you?</h4>
										</div>
									</div>
									<div class="wq_questionMediaCtr">
									
									</div>
									<div class="wq_questionAnswersCtr">
										<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"> <input type="radio" name="important_to" value="Status">Status</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												 
												<label class="wq_answerTxtCtr"> <input type="radio" name="important_to" value="Power">Power</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												  
												<label class="wq_answerTxtCtr"><input type="radio" name="important_to" value="Money">Money</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												  
												<label class="wq_answerTxtCtr"><input type="radio" name="important_to" value="Love">Love</label>
											</div>
										</div>
									</div>
									<div class="wq_triviaQuestionExplanation">
										<div class="wq_ExplanationHead"></div>
										<p class="wq_QuestionExplanationText"></p>
									</div>
								</div>
                            </div>
							
                        </div>
                    </div>
					  <div class="step w-slide">
					   <div class="step-counter" data-ix="show-content-onslide">Creating quiz 2/20</div>
                        <div class="collaborate-form-step1">
                           <div class="form-content">
                              <div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>If you meet a genie, what would be your wish?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												
												<label class="wq_answerTxtCtr"><input type="radio" name="your_wish" value="Beautiful Wife/Handsome Husband">Beautiful Wife/Handsome Husband</label>
											</div>
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												
												<label class="wq_answerTxtCtr"><input type="radio" name="your_wish" value="To be the PM of Country">To be the PM of Country</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												
												<label class="wq_answerTxtCtr"><input type="radio" name="your_wish" value="3 More Wishes">3 More Wishes</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
                           </div>
                          
                        </div>
                     </div>
					  <div class="step w-slide">
					<div class="step-counter" data-ix="show-content-onslide">Creating quiz 3/20</div>
					  
                        <div class="collaborate-form-step1">
                           <div class="form-content">
                              <div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>What would make you most happy?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"> <input type="radio" name="most_happy" value="Money">Money</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"> <input type="radio" name="most_happy" value="Love">Love</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"> <input type="radio" name="most_happy" value="Time with Friends">Time with Friends</label>
											</div>
										</div>
											</div>
										</div>
                           </div>
                        </div>
                     </div>
					  <div class="step w-slide">
					  <div class="step-counter" data-ix="show-content-onslide">Creating quiz 4/20</div>
                        <div class="collaborate-form-step1">
                           <div class="form-content">
                              <div class="wq_singleQuestionCtr">
									<div class="wq_questionTextWrapper quiz-clearfix">
										<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
											<h4>What would you do if you won a lottery?</h4>
										</div>
									</div>
									<div class="wq_questionMediaCtr">
										
									</div>
									<div class="wq_questionAnswersCtr">
									<div class="wq_answersWrapper">
									<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
										<label class="wq_answerTxtCtr"><input type="radio" name="won_a_lottery" value="Travel the world">Travel the world</label>
									</div>
								
									<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
										<label class="wq_answerTxtCtr"><input type="radio" name="won_a_lottery" value="Invest it">Invest it</label>
									</div>
								
									<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
										<label class="wq_answerTxtCtr"><input type="radio" name="won_a_lottery" value="Eat at fancy restaurants">Eat at fancy restaurants</label>
									</div>
								
									<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
										<label class="wq_answerTxtCtr"><input type="radio" name="won_a_lottery" value="Buy a sports car">Buy a sports car</label>
									</div>
								</div>
									</div>
									<div class="wq_triviaQuestionExplanation">
										<div class="wq_ExplanationHead"></div>
										<p class="wq_QuestionExplanationText"></p>
									</div>
								</div>
                           </div>
                           
                        </div>
                     </div>
                     <div class="step w-slide">
					 <div class="step-counter" data-ix="show-content-onslide">Creating quiz 5/20</div>
                        <div class="collaborate-form-step1">
                           <div class="form-content">
                             <div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>What type of movies do you like?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Romance">Romance</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Action">Action</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Thriller">Thriller</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Comedy">Comedy</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Horror">Horror</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Magic">Magic</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Fantasy">Fantasy</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Fantasy">Sci Fi</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Animated">Animated</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
                           </div>
                           
                        </div>
                     </div>
                     <div class="step w-slide">
					 <div class="step-counter" data-ix="show-content-onslide">Creating quiz 6/20</div>
                        <div class="collaborate-form-step1">
                           <div class="form-content">
                             <div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>Your favourite flavour is?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="favourite_flavour" value="Vanilla">Vanilla</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="favourite_flavour" value="Butter Scotch">Butter Scotch</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="favourite_flavour" value="Strawberry">Strawberry</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="favourite_flavour" value="Chocolate">Chocolate</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="favourite_flavour" value="Black Current">Black Current</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="favourite_flavour" value="Mango">Mango</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
                           </div>
                           
                        </div>
                     </div>
					 <div class="step w-slide">
					 <div class="step-counter" data-ix="show-content-onslide">Creating quiz 7/20</div>
                        <div class="collaborate-form-step1">
                           <div class="form-content">
                             <div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>What you are afraid of?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
										<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="afraid_of" value="High Places">High Places</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="afraid_of" value="Darkness">Darkness</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="afraid_of" value="Insects/Cockroaches/Rats/Flies">Insects/Cockroaches/Rats/Flies</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="afraid_of" value="Flying">Flying</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="afraid_of" value="Injection">Injection</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="afraid_of" value="Alone at Home">Alone at Home</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="afraid_of" value="Nothing">Nothing</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
							</div>
                           </div>
                           
                        </div>
                     </div>
					 
					 <div class="step w-slide">
					  <div class="step-counter" data-ix="show-content-onslide">Creating quiz 8/20</div>
                        <div class="collaborate-form-step1">
                           <div class="form-content">
                             <div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>What would you choose?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="you_choose" value="Fly like Superman">Fly like Superman</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="you_choose" value="Be invisible">Be invisible</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="you_choose" value="See Ghosts">See Ghosts</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="you_choose" value="See Ghosts">Read Minds of the people</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
                           </div>
                          
                        </div>
                     </div>
					 <div class="step w-slide">
					 <div class="step-counter" data-ix="show-content-onslide">Creating quiz 9/20</div>
                        <div class="collaborate-form-step1">
                           <div class="form-content">
                             <div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4> Which colour clothes you wear the most?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="cloth_colour" value="Red">Red</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="cloth_colour" value="Blue">Blue</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="cloth_colour" value="Orange">Orange</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="cloth_colour" value="Pink">Pink</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="cloth_colour" value="Yellow">Yellow</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="cloth_colour" value="Black">Black</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="cloth_colour" value="Green">Green</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="cloth_colour" value="White">White</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
                           </div>
                           
                        </div>
                     </div>
					 <div class="step w-slide">
					  <div class="step-counter" data-ix="show-content-onslide">Creating quiz 10/20</div>
                        <div class="collaborate-form-step1">
                           <div class="form-content">
                             <div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>What is your idea of a perfect evening?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="perfect_evening" value="Candle Light Dinner">Candle Light Dinner</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="perfect_evening" value="Movie with Friends">Movie with Friends</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="perfect_evening" value="Hit a Club">Hit a Club</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="perfect_evening" value="Cricket Match">Cricket Match</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="perfect_evening" value="Football Match">Football Match</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="perfect_evening" value="Travel">Travel</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="perfect_evening" value="Be Alone">Be Alone</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="perfect_evening" value="Sleeping">Sleeping</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
                           </div>
                          
                        </div>
                     </div>
					 <div class="step w-slide">
					 <div class="step-counter" data-ix="show-content-onslide">Creating quiz 11/20</div>
                        <div class="collaborate-form-step1">
                           <div class="form-content">
								<div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>Do you like Apple Iphone or an Android Smartphone?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="smart_phone" value="Apple Iphone">Apple Iphone</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="smart_phone" value="Android Smartphone">Android Smartphone</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
								</div>
                           </div>
                           
                        </div>
                     </div>
					 <div class="step w-slide">
					 <div class="step-counter" data-ix="show-content-onslide">Creating quiz 12/20</div>
                        <div class="collaborate-form-step1">
                           <div class="form-content">
                             <div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>Do you want your Wife/Husband to be the Hot/Smart or the Intelligent?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="intelligent" value="Hot/Smart">Hot/Smart</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="intelligent" value="Intelligent">Intelligent</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
                           </div>
                           
                        </div>
                     </div>
					 <div class="step w-slide">
					 <div class="step-counter" data-ix="show-content-onslide">Creating quiz 13/20</div>
                        <div class="collaborate-form-step1">
                           <div class="form-content">
                             <div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4> Where would you like to go with your soulmate?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_soulmate" value="Paris">Paris</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_soulmate" value="Bali">Bali</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_soulmate" value="Sonoma">Sonoma</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_soulmate" value="Venice">Venice</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_soulmate" value="Bahamas">Bahamas</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_soulmate" value="Cape Town">Cape Town</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_soulmate" value="Thailand">Thailand</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_soulmate" value="Greece">Greece</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_soulmate" value="Maldives">Maldives</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_soulmate" value="Mauritus">Mauritus</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
                           </div>
                           
                        </div>
                     </div>
					 <div class="step w-slide">
					 <div class="step-counter" data-ix="show-content-onslide">Creating quiz 14/20</div>
                        <div class="collaborate-form-step1">
                           <div class="form-content">
                             <div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>What you drink the most?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="most_drink" value="Tea">Tea</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="most_drink" value="Coffee">Coffee</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="most_drink" value="Milk">Milk</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="most_drink" value="Alcohol">Alcohol</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="most_drink" value="Green Tea">Green Tea</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="most_drink" value="Water">Water</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="most_drink" value="Juice">Juice</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
                           </div>
                           
                        </div>
                     </div>
					 <div class="step w-slide">
					 <div class="step-counter" data-ix="show-content-onslide">Creating quiz 15/20</div>
                        <div class="collaborate-form-step1">
                           <div class="form-content">
                             <div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>What is your favourite food?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="favourite_food" value="Pizza">Pizza</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="favourite_food" value="Burger">Burger</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="favourite_food" value="Noodles">Noodles</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="favourite_food" value="Salad">Salad</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="favourite_food" value="Pasta">Pasta</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="favourite_food" value="Simple Food">Simple Food</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
                           </div>
                           
                        </div>
                     </div>
					 <div class="step w-slide">
					 <div class="step-counter" data-ix="show-content-onslide">Creating quiz 16/20</div>
                        <div class="collaborate-form-step1">
                           <div class="form-content">
                             <div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>Which fast food restaurant do you prefer the most</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="fast_food" value="Pizza Hut">Pizza Hut</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="fast_food" value="Dominos">Dominos</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="fast_food" value="Subway">Subway</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="fast_food" value="McDonald's">McDonald's</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="fast_food" value="KFC">KFC</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="fast_food" value="Burger King">Burger King</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="fast_food" value="Wendy's">Wendy's</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="fast_food" value="Taco Bell">Taco Bell</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="fast_food" value="Baskin Robbins">Baskin Robbins</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
                           </div>
                           
                        </div>
                     </div>
					 <div class="step w-slide">
					 <div class="step-counter" data-ix="show-content-onslide">Creating quiz 17/20</div>
                        <div class="collaborate-form-step1">
                           <div class="form-content">
						   <div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>Which animal do you dream to pet?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="dream_to_pet" value="A Dog">A Dog</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="dream_to_pet" value="Love Birds">Love Birds</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="dream_to_pet" value="A Rabbit">A Rabbit</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="dream_to_pet" value="A Cat">A Cat</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="dream_to_pet" value="A Lion">A Lion</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										
							</div>
                           
                        </div>
                     </div>
					 <div class="step w-slide">
					 <div class="step-counter" data-ix="show-content-onslide">Creating quiz 18/20</div>
                        <div class="collaborate-form-step1">
                           <div class="form-content">
						   <div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>In your freetime, where would you go?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="freetime" value="Gym">Gym</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="freetime" value="Sleeping">Sleeping</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="freetime" value="Shopping">Shopping</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="freetime" value="Swimming">Swimming</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="freetime" value="Reading">Reading</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="freetime" value="Travel">Travel</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="freetime" value="Movie">Movie</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										
							</div>
                           
                        </div>
                     </div>
					 <div class="step w-slide">
					 <div class="step-counter" data-ix="show-content-onslide">Creating quiz 19/20</div>
                        <div class="collaborate-form-step1">
                           <div class="form-content">
						   <div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>What will you choose?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_choice" value="Change the Past">Change the Past</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_choice" value="See the Future">See the Future</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										
							</div>
                           
                        </div>
                     </div>
					 <div class="step w-slide">
					 <div class="step-counter" data-ix="show-content-onslide">Creating quiz 20/20</div>
                        <div class="collaborate-form-step1">
                           <div class="form-content">
						   <div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>Which songs you like to listen to?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="listen_songs" value="Romantic Songs">Romantic Songs</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="listen_songs" value="Party Songs">Party Songs</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="listen_songs" value="Pop">Pop</label>
											</div>
									
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="listen_songs" value="Sufi">Sufi</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="listen_songs" value="Devotional">Devotional</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="listen_songs" value="Classical">Classical</label>
											</div>
										</div>
											</div>
										   <div class="collaborate-form-step1">
                           <div class="final form-content1">
							  <input class="submit-button w-button" type="submit" name="submit_quiz" value="Submit Quiz">
                           </div>
                        </div>
										</div>
										
							</div>
                           
                        </div>
                     </div>
                  </div>
                 
               </div>
            </form>
            <div class="success-message w-form-done">
               <div>Thanks! I have received your form submission, I'll get back to you shortly!</div>
            </div>
            <div class="error-message w-form-fail">
               <div>Oops! Something went wrong while submitting the form</div>
            </div>
      </div>
   </div>
<script>
jQuery( document ).ready(function() {
   jQuery('.wq_answerTxtCtr input').click(function() {
	   var answer33 = jQuery('.wq_answerTxtCtr input').parent();
	   console.log(answer33); 
	   jQuery(answer33).css("background-color", "#F2E3DD");
   if(jQuery(this).is(':checked')) { 
	   var answer22 = jQuery(this).parent();
	   jQuery(answer22).css("background-color", "#00B639");
	   console.log("jhghgjhgjhgjhgh"); 
   }
});
});

</script>   
					<?php	
					return ob_get_clean();  
					
			}
		

		/**
		 * Register Quiz post type
		 */
		public static function register_post_type() {

			$labels = array(
				'name'               => __( 'WP Quiz', 'wp-quiz' ),
				'menu_name'          => __( 'WP Quiz', 'wp-quiz' ),
				'singular_name'      => __( 'WP Quiz', 'wp-quiz' ),
				'name_admin_bar'     => _x( 'WP Quiz', 'name admin bar', 'wp-quiz' ),
				'all_items'          => __( 'All Quizzes', 'wp-quiz' ),
				'search_items'       => __( 'Search Quizzes', 'wp-quiz' ),
				'add_new'            => _x( 'Add New', 'quiz', 'wp-quiz' ),
				'add_new_item'       => __( 'Add New WP Quiz', 'wp-quiz' ),
				'new_item'           => __( 'New Quiz', 'wp-quiz' ),
				'view_item'          => __( 'View Quiz', 'wp-quiz' ),
				'edit_item'          => __( 'Edit Quiz', 'wp-quiz' ),
				'not_found'          => __( 'No Quizzes Found.', 'wp-quiz' ),
				'not_found_in_trash' => __( 'WP Quiz not found in Trash.', 'wp-quiz' ),
				'parent_item_colon'  => __( 'Parent Quiz', 'wp-quiz' ),
			);

			$args = array(
				'labels'             => $labels,
				'description'        => __( 'Holds the quizzes and their data.', 'wp-quiz' ),
				'menu_position'      => 5,
				'menu_icon'          => 'dashicons-editor-help',
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => true,
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'supports'           => array( 'title', 'author', 'thumbnail', 'excerpt' ),
			);

			register_post_type( 'wp_quiz', $args );
		}

		/**
		 * Shortcode used to display quiz
		 *
		 * @return string HTML output of the shortcode
		 */
		public function register_shortcode( $atts ) {

			if ( ! isset( $atts['id'] ) ) {
				return false;
			}

			// we have an ID to work with
			$quiz = get_post( $atts['id'] );

			// check if ID is correct
			if ( ! $quiz || 'wp_quiz' !== $quiz->post_type ) {
				return "<!-- wp_quiz {$atts['id']} not found -->";
			}

			// lets go
			$this->set_quiz( $atts['id'] );
			$this->quiz->enqueue_scripts();

			return $this->quiz->render_public_quiz();
		}

		/**
		 * Set the current quiz
		 */
		public function set_quiz( $id ) {

			$quiz_type  = get_post_meta( $id, 'quiz_type', true );
			$quiz_type  = str_replace( '_quiz', '', $quiz_type );
			$quiz_type  = 'WP_Quiz_' . ucwords( $quiz_type ) . '_Quiz';
			$this->quiz = new $quiz_type( $id );
		}

		/**
		 * [create_quiz_page description]
		 * @param  [type] $content [description]
		 * @return [type]          [description]
		 */
		public function create_quiz_page( $content ) {

			global $post;

			if ( 'wp_quiz' !== $post->post_type ) {
				return $content;
			}

			if ( ! is_single() ) {
				return $content;
			}

			$quiz_html = $this->register_shortcode( array( 'id' => $post->ID ) );

			return $quiz_html . $content;
		}

		public function check_image_file() {

			$output = array( 'status' => 1 );
			$check  = false;
			if ( @getimagesize( $_POST['url'] ) ) {
				$check = true;
			}

			$output['check'] = $check;
			wp_send_json( $output );
		}

		public function check_video_file() {

			$output  = array( 'status' => 1 );
			$check   = false;
			$id      = $_POST['video_id'];
			$url     = "//www.youtube.com/oembed?url=http://www.youtube.com/watch?v=$id&format=json";
			$headers = get_headers( $url );

			if ( '404' !== substr( $headers[0], 9, 3 ) ) {
				$check = true;
			}

			$output['check'] = $check;
			wp_send_json( $output );
		}

		public function get_debug_log() {
			$page = new WP_Quiz_Page_Support();
			$page->get_debug_log();
		}

		public static function activate_plugin() {

			// Don't activate on anything less than PHP 5.4.0 or WordPress 3.4
			if ( version_compare( PHP_VERSION, '5.4.0', '<' ) || version_compare( get_bloginfo( 'version' ), '3.4', '<' ) || ! function_exists( 'spl_autoload_register' ) ) {
				require_once ABSPATH . 'wp-admin/includes/plugin.php';
				deactivate_plugins( basename( __FILE__ ) );
				wp_die( __( 'WP Quiz requires PHP version 5.4.0 with spl extension or greater and WordPress 3.4 or greater.', 'wp-quiz' ) );
			}

			// Dont't activate if wp quiz pro is active
			if ( defined( 'WP_QUIZ_PRO_VERSION' ) ) {
				deactivate_plugins( basename( __FILE__ ) );
				wp_die( __( 'Please deactivate WP Quiz Pro plugin', 'wp-quiz' ) );
			}

			include( 'inc/activate-plugin.php' );
		}

		public function fb_share_fix() {

			$data   = array_map( 'urldecode', $_GET );
			$result = get_post_meta( $data['id'], 'results', true );
			$result = isset( $result[ $data['rid'] ] ) ? $result[ $data['rid'] ] : array();

			// Picture
			if ( 'r' === $data['pic'] ) {
				$data['source'] = $result['image'];
			} elseif ( 'f' === $data['pic'] ) {
				$data['source'] = wp_get_attachment_url( get_post_thumbnail_id( $data['id'] ) );
			} elseif ( ( substr( $data['pic'], 0, 6 ) === 'image-' ) ) {
				$upload_dir            = wp_upload_dir();
				$upload_dir['baseurl'] = $upload_dir['baseurl'] . '/wp_quiz-result-images';
				$data['source']        = $upload_dir['baseurl'] . '/' . $data['pic'] . '.png';
			} else {
				$data['source'] = false;
			}

			// Description
			if ( 'r' === $data['desc'] ) {
				$data['description'] = $result['desc'];
			} elseif ( 'e' === $data['desc'] ) {
				$data['description'] = get_post_field( 'post_excerpt', $data['id'] );
			} else {
				$data['description'] = false;
			}

			$settings = get_option( 'wp_quiz_default_settings' );
			$url      = ( is_ssl() ? 'https' : 'http' ) . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

			global $post;
			$pid          = $post ? $post->ID : $data['id'];
			$original_url = get_permalink( $pid );
			?>
			<html>
				<head>
					<title><?php wp_title( '' ); ?></title>
					<meta property="fb:app_id" content="<?php echo $settings['defaults']['fb_app_id']; ?>">
					<meta property="og:type" content="website">
					<meta property="og:url" content="<?php echo esc_url( $url ); ?>">
					<meta name="twitter:card" content="summary_large_image">
					<?php
					if ( ! empty( $data['text'] ) ) :
						$title = get_the_title( $pid );
						$text  = esc_attr( $data['text'] );

						$title = $title === $text ? $title : $title . ' - ' . $text;
					?>
					<meta property="og:title" content="<?php echo $title; ?>">
					<meta property="twitter:title" content="<?php echo $title; ?>">
					<?php endif; ?>
					<?php if ( ! empty( $data['source'] ) ) : ?>
					<meta property="og:image" content="<?php echo esc_url( $data['source'] ); ?>">
					<meta property="twitter:image" content="<?php echo esc_url( $data['source'] ); ?>">
						<?php list( $img_width, $img_height ) = getimagesize( $data['source'] ); ?>
						<?php if ( isset( $img_width ) && $img_width ) : ?>
						<meta property="og:image:width" content="<?php echo $img_width; ?>">
						<?php endif; ?>
						<?php if ( isset( $img_height ) && $img_height ) : ?>
						<meta property="og:image:height" content="<?php echo $img_height; ?>">
						<?php endif; ?>
					<?php endif; ?>
					<?php if ( ! empty( $data['description'] ) ) : ?>
					<meta property="og:description" content="<?php echo esc_attr( $data['description'] ); ?>">
					<meta property="twitter:description" content="<?php echo esc_attr( $data['description'] ); ?>">
					<?php endif; ?>
					<meta http-equiv="refresh" content="0;url=<?php echo esc_url( $original_url ); ?>">
				</head>
			<body>
				Redirecting please wait....
			</body>
			</html>
			<?php
			exit;
		}

		public function inline_script() {
			$settings = get_option( 'wp_quiz_default_settings' );
			?>
				<script>
				<?php if ( ! empty( $settings['defaults']['fb_app_id'] ) ) { ?>
					window.fbAsyncInit = function() {
						FB.init({
							appId    : '<?php echo $settings['defaults']['fb_app_id']; ?>',
							xfbml    : true,
							version  : 'v2.9'
						});
					};

					(function(d, s, id){
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) {return;}
						js = d.createElement(s); js.id = id;
						js.src = "//connect.facebook.net/en_US/sdk.js";
						fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));
				<?php } ?>
				</script>
			<?php
			if ( is_singular( array( 'wp_quiz' ) ) && isset( $settings['defaults']['share_meta'] ) && 1 === $settings['defaults']['share_meta'] ) {
				global $post, $wpseo_og;
				$twitter_desc = $og_desc = str_replace( array( "\r", "\n" ), '', strip_tags( $post->post_excerpt ) );
				if ( defined( 'WPSEO_VERSION' ) ) {
					remove_action( 'wpseo_head', array( $wpseo_og, 'opengraph' ), 30 );
					remove_action( 'wpseo_head', array( 'WPSEO_Twitter', 'get_instance' ), 40 );
					//use description from yoast
					$twitter_desc = get_post_meta( $post->ID, '_yoast_wpseo_twitter-description', true );
					$og_desc      = get_post_meta( $post->ID, '_yoast_wpseo_opengraph-description', true );
				}
				?>
				<meta name="twitter:title" content="<?php echo get_the_title(); ?>">
				<meta name="twitter:description" content="<?php echo $twitter_desc; ?>">
				<meta name="twitter:domain" content="<?php echo esc_url( site_url() ); ?>">
				<meta property="og:url" content="<?php the_permalink(); ?>" />
				<meta property="og:title" content="<?php echo get_the_title(); ?>" />
				<meta property="og:description" content="<?php echo $og_desc; ?>" />
				<?php
				if ( has_post_thumbnail() ) {
					$thumb_id        = get_post_thumbnail_id();
					$thumb_url_array = wp_get_attachment_image_src( $thumb_id, 'full', true );
					$thumb_url       = $thumb_url_array[0];
					?>
					<meta name="twitter:card" content="summary_large_image">
					<meta name="twitter:image:src" content="<?php echo $thumb_url; ?>">
					<meta property="og:image" content="<?php echo $thumb_url; ?>" />
					<meta itemprop="image" content="<?php echo $thumb_url; ?>">
				<?php
				}
			}
		}

		/**
		 * Get plugin directory.
		 * @return string
		 */
		public function plugin_dir() {
			if ( is_null( $this->plugin_dir ) ) {
				$this->plugin_dir = untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/';
			}
			return $this->plugin_dir;
		}

		/**
		 * Get plugin uri.
		 * @return string
		 */
		public function plugin_url() {
			if ( is_null( $this->plugin_url ) ) {
				$this->plugin_url = untrailingslashit( plugin_dir_url( __FILE__ ) ) . '/';
			}
			return $this->plugin_url;
		}
	}

	/**
	 * Main instance of WP_Quiz_Plugin.
	 *
	 * Returns the main instance of WP_Quiz_Plugin to prevent the need to use globals.
	 *
	 * @return WP_Quiz_Plugin
	 */

	function wp_quiz() {
		return WP_Quiz_Plugin::get_instance();
	}

endif;

add_action( 'plugins_loaded', 'wp_quiz', 10 );
register_activation_hook( __FILE__, array( 'WP_Quiz_Plugin', 'activate_plugin' ) );
