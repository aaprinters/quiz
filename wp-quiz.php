				<?php
				/*
				 * Plugin Name: WP Quiz
				 * Plugin URI:  https://mythemeshop.com/plugins/wp-quiz/
				 * Description: WP Quiz makes it incredibly easy to add professional, multimedia quizzes to any website! Fully feature rich and optimized for social sharing. Make your site more interactive!
				 * Version:     1.1.5
				 * Author:      MyThemeShop
				 * Author URI:  https://mythemeshop.com/
				 *
				 * Text Domain: wp-quiz
				 * Domain Path: /languages/
				 */

				// If this file is called directly, abort.
				if ( ! defined( 'WPINC' ) ) {
					die;
				}

				if ( ! class_exists( 'WP_Quiz_Plugin' ) ) :


					/**
					 * Register the plugin.
					 *
					 * Display the administration panel, insert JavaScript etc.
					 */
					class WP_Quiz_Plugin {

						/**
						 * Hold plugin version
						 * @var string
						 */
						public $version = '1.1.5';

						/**
						 * Hold an instance of WP_Quiz_Plugin class.
						 *
						 * @var WP_Quiz_Plugin
						 */
						protected static $instance = null;

						/**
						 * @var WP Quiz
						 */
						public $quiz = null;

						/**
						 * Plugin url.
						 * @var string
						 */
						private $plugin_url = null;

						/**
						 * Plugin path.
						 * @var string
						 */
						private $plugin_dir = null;

						/**
						 * Main WP_Quiz_Plugin instance.
						 * @return WP_Quiz_Plugin - Main instance.
						 */
						public static function get_instance() {

							if ( is_null( self::$instance ) ) {
								self::$instance = new WP_Quiz_Plugin;
							}

							return self::$instance;
						}

						/**
						 * You cannot clone this class.
						 */
						public function __clone() {
							_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'wp-quiz' ), $this->version );
						}

						/**
						 * You cannot unserialize instances of this class.
						 */
						public function __wakeup() {
							_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'wp-quiz' ), $this->version );
						}

						/**
						 * Constructor
						 */
						public function __construct() {
							$this->includes();
							$this->hooks();
							$this->setup_shortcode();
						}

						/**
						 * Load required classes
						 */
						private function includes() {

							//auto loader
							spl_autoload_register( array( $this, 'autoloader' ) );

							new WP_Quiz_Admin;
						}

						/**
						 * Autoload classes
						 */
						public function autoloader( $class ) {

							$dir             = $this->plugin_dir() . 'inc' . DIRECTORY_SEPARATOR;
							$class_file_name = 'class-' . str_replace( array( 'wp_quiz_', '_' ), array( '', '-' ), strtolower( $class ) ) . '.php';

							if ( file_exists( $dir . $class_file_name ) ) {
								require $dir . $class_file_name;
							}
						}

						/**
						 * Register the [wp_quiz] shortcode.
						 */
						private function setup_shortcode() {
							add_shortcode( 'wp_quiz', array( $this, 'register_shortcode' ) );
						}

						/**
						 * Hook WP Quiz into WordPress
						 */
						private function hooks() {
							

							// Common
							add_action( 'init', array( $this, 'load_plugin_textdomain' ) );
							add_action( 'init', array( $this, 'register_post_type' ) );

							// Frontend
							add_action( 'wp_head', array( $this, 'inline_script' ), 1 );
							add_filter( 'the_content', array( $this, 'create_quiz_page' ) );

							// Ajax
							add_action( 'wp_ajax_check_image_file', array( $this, 'check_image_file' ) );
							add_action( 'wp_ajax_check_video_file', array( $this, 'check_video_file' ) );

							add_action( 'wp_ajax_wpquiz_get_debug_log', array( $this, 'get_debug_log' ) );

							// FB SDK version 2.9 fix
							if ( isset( $_GET['fbs'] ) && ! empty( $_GET['fbs'] ) ) {
								add_action( 'template_redirect', array( $this, 'fb_share_fix' ) );
							}
						}

						/**
						 * Initialise translations
						 */
						public function load_plugin_textdomain() {
							load_plugin_textdomain( 'wp-quiz', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
							add_shortcode( 'quiz_front', array( $this, 'contact_form' ));			
						}
						
						
					//Custom Front End Form.

					function contact_form( $atts ){
							
									if(isset($_POST["submit_quiz"])){
											$important_to = $_POST['important_to'];
											$your_wish = $_POST['your_wish'];
											$my_quiz = array(
												'post_title'    => 'My Quiz',
												'post_content'  => 'This is my Quiz.',
												'post_status'   => 'publish',
												'post_type'   => 'wp_quiz'
											);
											$new_quiz_type = 'trivia';
											// Insert the post into the database.
											$quiz_id = wp_insert_post( $my_quiz);
											update_post_meta( $quiz_id, 'quiz_type', $new_quiz_type );
										/* 	 
						Array(
				            [title] => What is more important to you?
				            [mediaType] => image
				            [image] => 
				            [imageCredit] => 
				            [video] => 
				            [imagePlaceholder] => 
				            [desc] => 
				            [answers] => Array
				                (
				                    [0] => Array
				                        (
				                            [title] => Status
				                            [image] => 
				                            [isCorrect] => 0
				                        )

				                    [1] => Array
				                        (
				                            [title] => Power
				                            [image] => 
				                            [isCorrect] => 0
				                        )

				                    [2] => Array
				                        (
				                            [title] => MOney
				                            [image] => 
				                            [isCorrect] => 0
				                        )

				                    [3] => Array
				                        (
				                            [title] => Love
				                            [image] => 
				                            [isCorrect] => 1
				                        )

				                )
				 
				        ) */
												$answer1 = array(
															0 => array(
																'title' => 'Love',
																'image' => '',
																'isCorrect' => '0'
															),
															1 => array(
																'title' => 'Status',
																'image' => '',
																'isCorrect' => '1'
															),
															2 => array(
																'title' => 'Money',
																'image' => '',
																'isCorrect' => '0'
															),
															3 => array(
																'title' => 'Power',
																'image' => '',
																'isCorrect' => '0'
															),
												);
												$new_answer1=array();
												$answer_arr1=array();
												foreach($answer1 as $answer_arr_key=>$answer_arr1){
													foreach($answer_arr1 as $answer_key=>$answer_value){
														$answer_title_upp = $answer_arr1['title'];
														$answer_title = strtolower($answer_title_upp);
														if($important_to == $answer_title){
															$answer_arr1['isCorrect'] = '1';
														}else{
															$answer_arr1['isCorrect'] = '0';
														}
														
													}
													$new_answer1[$answer_arr_key]=$answer_arr1;	
												}
												$answer2 = array(
															0 => array(
																'title' => '1 Million Dollar',
																'image' => '',
																'isCorrect' => '0'
															),
															1 => array(
																'title' => 'Beautiful Wife/Handsome Husband',
																'image' => '',
																'isCorrect' => '1'
															),
															2 => array(
																'title' => 'To be the PM of Country',
																'image' => '',
																'isCorrect' => '0'
															),
															3 => array(
																'title' => '3 More Wishes',
																'image' => '',
																'isCorrect' => '0'
															),
															
												);
												$new_answer2=array();
												$answer_arr2=array();
												foreach($answer2 as $answer_arr_key=>$answer_arr2){
													foreach($answer_arr2 as $answer_key=>$answer_value){
														$answer_title = $answer_arr2['title'];
														if($your_wish == $answer_title){
															$answer_arr2['isCorrect'] = '1';
														}else{
															$answer_arr2['isCorrect'] = '0';
														}
														
													}
													$new_answer2[$answer_arr_key]=$answer_arr2;	
												}
												$answer3 = array(
															0 => array(
																'title' => '1 million',
																'image' => '',
																'isCorrect' => '0'
															),
															1 => array(
																'title' => '3 million',
																'image' => '',
																'isCorrect' => '1'
															),
															2 => array(
																'title' => '4 million',
																'image' => '',
																'isCorrect' => '0'
															),
												);
											$questions = array(
															0 => array(
																'title' => 'What is more important to you?',
																'mediaType' => '',
																'answers' => $new_answer1
															),
															1 => array(
																'title' => 'If you meet a genie, what would be your wish?',
																'mediaType' => '',
																'answers' => $new_answer2
															),
															2 => array(
																'title' => 'What would make you most happy?',
																'mediaType' => '',
																'answers' => $answer3
															),
															

													);
											update_post_meta( $quiz_id, 'questions', $questions );
				/* Array
				(
				[0] => Array
					(
						[title] => 0 match
						[image] => 
						[min] => 0
						[max] => 0
						[desc] => 0 match
					)
					) */
											$results = array(
															0 => array(
																'title' => '0 match',
																'image' => '',
																'min' => '0',
																'max' => '0',
																'desc' => '0'
															),
															1 => array(
																'title' => '1 match',
																'image' => '',
																'min' => '1',
																'max' => '1',
																'desc' => '0'
															),
															2 => array(
																'title' => '2 match',
																'image' => '',
																'min' => '2',
																'max' => '2',
																'desc' => '0'
															)
												);
											update_post_meta( $quiz_id, 'results', $results );
										
									}				
				ob_start();?>				
				<div class="wq_quizCtr multiple trivia_quiz" data-current-question="0" data-questions-answered="0" data-questions="20" data-transition_in="fade" data-transition_out="fade" data-correct-answered="0" data-quiz-pid="13" data-share-url="http://quiz.logicsbuffer.com/wp_quiz/test11/" data-post-title="test11" data-retake-quiz="0" data-question-layout="multiple" data-featured-image="" data-excerpt="" data-ajax-url="http://quiz.logicsbuffer.com/wp-admin/admin-ajax.php" data-auto-scroll="1">
				   	<div class="wq_quizProgressBarCtr">
				        <!-- progress bar -->
				<div class="wq_quizProgressBarCtr" style="display:block">
				<div class="wq_quizProgressBar">
				<span style="background-color:#00c479" class="wq_quizProgressValue"></span>
				</div>
				</div>
				<!--// progress bar-->
				   	</div>
					<form method="post" action="">
				   	<div class="wq_questionsCtr">
						<div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:block;">
						<div class="wq_singleQuestionCtr">
							<div class="wq_questionTextWrapper quiz-clearfix">
							<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
								<h4>What is more important to you?</h4>
							</div>
							</div>
							<div class="wq_questionMediaCtr">
								
							</div>
							<div class="wq_questionAnswersCtr">
								<div class="wq_answersWrapper">
							<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
								
								<label class="wq_answerTxtCtr"> <input type="radio" name="important_to" value="status">Status</label>
							</div>
						
							<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
								 
								<label class="wq_answerTxtCtr"> <input type="radio" name="important_to" value="power">Power</label>
							</div>
						
							<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
								  
								<label class="wq_answerTxtCtr"><input type="radio" name="important_to" value="money">Money</label>
							</div>
						
							<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
								  
								<label class="wq_answerTxtCtr"><input type="radio" name="important_to" value="love">Love</label>
							</div>
						</div>
							</div>
							<div class="wq_triviaQuestionExplanation">
								<div class="wq_ExplanationHead"></div>
								<p class="wq_QuestionExplanationText"></p>
							</div>
						</div>
					
										<div class="wq_continue">
											<button class="wq_btn-continue" style="background-color:#00c479">Continue &gt;&gt;</button>
										</div>
						</div>
								
									<div class="wq_singleQuestionWrapper wq_IsTrivia">
										<div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>If you meet a genie, what would be your wish?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label><input type="radio" name="your_wish">Some option</label>
											</div>
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												
												<label class="wq_answerTxtCtr"><input type="radio" name="your_wish" value="Beautiful Wife/Handsome Husband">Beautiful Wife/Handsome Husband</label>
											</div>
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												
												<label class="wq_answerTxtCtr"><input type="radio" name="your_wish" value="To be the PM of Country">To be the PM of Country</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												
												<label class="wq_answerTxtCtr"><input type="radio" name="your_wish" value="3 More Wishes">3 More Wishes</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										<div class="wq_continue" style="display:none1;">
											<button class="wq_btn-continue" style="background-color:#00c479">Continue &gt;&gt;</button>
										</div>
									</div>
								
									<div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none1;">
										<div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>What would make you most happy?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"> <input type="radio" name="most_happy" value="Money">Money</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"> <input type="radio" name="most_happy" value="Love">Love</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"> <input type="radio" name="most_happy" value="Time with Friends">Time with Friends</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText">What would make you most happy?</p>
											</div>
										</div>
										<div class="wq_continue" style="display:none1;">
											<button class="wq_btn-continue" style="background-color:#00c479">Continue &gt;&gt;</button>
										</div>
									</div>
								
									<div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none1;">
										<div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>What would you do if you won a lottery?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
											<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="won_a_lottery" value="Travel the world">Travel the world</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="won_a_lottery" value="Invest it">Invest it</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="won_a_lottery" value="Eat at fancy restaurants">Eat at fancy restaurants</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="won_a_lottery" value="Buy a sports car">Buy a sports car</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										<div class="wq_continue" style="display:none1;">
											<button class="wq_btn-continue" style="background-color:#00c479">Continue &gt;&gt;</button>
										</div>
									</div>
								
									<div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none1;">
										<div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>What type of movies do you like?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Romance">Romance</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Action">Action</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Thriller">Thriller</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Comedy">Comedy</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Horror">Horror</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Magic">Magic</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Fantasy">Fantasy</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Fantasy">Sci Fi</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Animated">Animated</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										<div class="wq_continue" style="display:none1;">
											<button class="wq_btn-continue" style="background-color:#00c479">Continue &gt;&gt;</button>
										</div>
									</div>
								
									<div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none1;">
										<div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>Your favourite flavour is?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Vanilla">Vanilla</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Butter Scotch">Butter Scotch</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Strawberry">Strawberry</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Chocolate">Chocolate</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Black Current">Black Current</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="movies_like" value="Mango">Mango</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										<div class="wq_continue" style="display:none1;">
											<button class="wq_btn-continue" style="background-color:#00c479">Continue &gt;&gt;</button>
										</div>
									</div>
								
									<div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none1;">
										<div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>What you are afraid of?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="afraid_of" value="High Places">High Places</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="afraid_of" value="Darkness">Darkness</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="afraid_of" value="Insects/Cockroaches/Rats/Flies">Insects/Cockroaches/Rats/Flies</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="afraid_of" value="Flying">Flying</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="afraid_of" value="Injection">Injection</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="afraid_of" value="Alone at Home">Alone at Home</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="afraid_of" value="Nothing">Nothing</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										<div class="wq_continue" style="display:none1;">
											<button class="wq_btn-continue" style="background-color:#00c479">Continue &gt;&gt;</button>
										</div>
									</div>
								
									<div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none1;">
										<div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>What would you choose?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="you_choose" value="Fly like Superman">Fly like Superman</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="you_choose" value="Be invisible">Be invisible</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="you_choose" value="See Ghosts">See Ghosts</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="you_choose" value="See Ghosts">Read Minds of the people</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										<div class="wq_continue" style="display:none1;">
											<button class="wq_btn-continue" style="background-color:#00c479">Continue &gt;&gt;</button>
										</div>
									</div>
								
									<div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none1;">
										<div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4> Which colour clothes you wear the most?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="cloth_colour" value="Red">Red</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="cloth_colour" value="Blue">Blue</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="cloth_colour" value="Orange">Orange</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="cloth_colour" value="Pink">Pink</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="cloth_colour" value="Yellow">Yellow</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="cloth_colour" value="Black">Black</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="cloth_colour" value="Green">Green</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="cloth_colour" value="White">White</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										<div class="wq_continue" style="display:none1;">
											<button class="wq_btn-continue" style="background-color:#00c479">Continue &gt;&gt;</button>
										</div>
									</div>
								
									<div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none1;">
										<div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>What is your idea of a perfect evening?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="perfect_evening" value="Candle Light Dinner">Candle Light Dinner</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="perfect_evening" value="Movie with Friends">Movie with Friends</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="perfect_evening" value="Hit a Club">Hit a Club</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="perfect_evening" value="Cricket Match">Cricket Match</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="perfect_evening" value="Football Match">Football Match</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="perfect_evening" value="Travel">Travel</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="perfect_evening" value="Be Alone">Be Alone</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="perfect_evening" value="Sleeping">Sleeping</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										<div class="wq_continue" style="display:none1;">
											<button class="wq_btn-continue" style="background-color:#00c479">Continue &gt;&gt;</button>
										</div>
									</div>
								
									<div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none1;">
										<div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>Do you like Apple Iphone or an Android Smartphone?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="smart_phone" value="Apple Iphone">Apple Iphone</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="smart_phone" value="Android Smartphone">Android Smartphone</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										<div class="wq_continue" style="display:none1;">
											<button class="wq_btn-continue" style="background-color:#00c479">Continue &gt;&gt;</button>
										</div>
									</div>
								
									<div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none1;">
										<div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>Do you want your Wife/Husband to be the Hot/Smart or the Intelligent?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="intelligent" value="Smart">Hot/Smart</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="intelligent" value="Intelligent">Intelligent</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										<div class="wq_continue" style="display:none1;">
											<button class="wq_btn-continue" style="background-color:#00c479">Continue &gt;&gt;</button>
										</div>
									</div>
								
									<div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none1;">
										<div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4> Where would you like to go with your soulmate?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_soulmate" value="Paris">Paris</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_soulmate" value="Bali">Bali</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_soulmate" value="Sonoma">Sonoma</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_soulmate" value="Venice">Venice</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_soulmate" value="Bahamas">Bahamas</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_soulmate" value="Cape Town">Cape Town</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_soulmate" value="Thailand">Thailand</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_soulmate" value="Greece">Greece</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_soulmate" value="Maldives">Maldives</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_soulmate" value="Mauritus">Mauritus</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										<div class="wq_continue" style="display:none1;">
											<button class="wq_btn-continue" style="background-color:#00c479">Continue &gt;&gt;</button>
										</div>
									</div>
								
									<div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none1;">
										<div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>What you drink the most?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="most_drink" value="Tea">Tea</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="most_drink" value="Coffee">Coffee</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="most_drink" value="Milk">Milk</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="most_drink" value="Alcohol">Alcohol</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="most_drink" value="Green Tea">Green Tea</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="most_drink" value="Water">Water</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="most_drink" value="Juice">Juice</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										<div class="wq_continue" style="display:none1;">
											<button class="wq_btn-continue" style="background-color:#00c479">Continue &gt;&gt;</button>
										</div>
									</div>
								
									<div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none1;">
										<div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>What is your favourite food?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="favourite_food" value="Pizza">Pizza</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="favourite_food" value="Burger">Burger</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="favourite_food" value="Noodles">Noodles</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="favourite_food" value="Salad">Salad</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="favourite_food" value="Pasta">Pasta</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="favourite_food" value="Simple Food">Simple Food</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										<div class="wq_continue" style="display:none1;">
											<button class="wq_btn-continue" style="background-color:#00c479">Continue &gt;&gt;</button>
										</div>
									</div>
								
									<div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none1;">
										<div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>Which fast food restaurant do you prefer the most</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="fast_food" value="Pizza Hut">Pizza Hut</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="fast_food" value="Dominos">Dominos</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="fast_food" value="Subway">Subway</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="fast_food" value="McDonald's">McDonald's</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="fast_food" value="KFC">KFC</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="fast_food" value="Burger King">Burger King</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="fast_food" value="Wendy's">Wendy's</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="fast_food" value="Taco Bell">Taco Bell</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="fast_food" value="Baskin Robbins">Baskin Robbins</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										<div class="wq_continue" style="display:none1;">
											<button class="wq_btn-continue" style="background-color:#00c479">Continue &gt;&gt;</button>
										</div>
									</div>
								
									<div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none1;">
										<div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>Which animal do you dream to pet?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="dream_to_pet" value="A Dog">A Dog</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="dream_to_pet" value="Love Birds">Love Birds</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="dream_to_pet" value="A Rabbit">A Rabbit</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="dream_to_pet" value="A Cat">A Cat</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="dream_to_pet" value="A Lion">A Lion</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										<div class="wq_continue" style="display:none1;">
											<button class="wq_btn-continue" style="background-color:#00c479">Continue &gt;&gt;</button>
										</div>
									</div>
								
									<div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none1;">
										<div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>In your freetime, where would you go?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="freetime" value="Gym">Gym</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="freetime" value="Sleeping">Sleeping</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="freetime" value="Shopping">Shopping</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="freetime" value="Swimming">Swimming</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="freetime" value="Reading">Reading</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="freetime" value="Travel">Travel</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="freetime" value="Movie">Movie</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										<div class="wq_continue" style="display:none1;">
											<button class="wq_btn-continue" style="background-color:#00c479">Continue &gt;&gt;</button>
										</div>
									</div>
								
									<div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none1;">
										<div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>What will you choose?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_choice" value="Change the Past">Change the Past</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="your_choice" value="See the Future">See the Future</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										<div class="wq_continue" style="display:none1;">
											<button class="wq_btn-continue" style="background-color:#00c479">Continue &gt;&gt;</button>
										</div>
									</div>
								
									<div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none1;">
										<div class="wq_singleQuestionCtr">
											<div class="wq_questionTextWrapper quiz-clearfix">
												<div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#444;">
													<h4>Which songs you like to listen to?</h4>
												</div>
											</div>
											<div class="wq_questionMediaCtr">
												
											</div>
											<div class="wq_questionAnswersCtr">
												<div class="wq_answersWrapper">
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="1" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="listen_songs" value="Romantic Songs">Romantic Songs</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="listen_songs" value="Party Songs">Party Songs</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="listen_songs" value="Pop">Pop</label>
											</div>
									
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="listen_songs" value="Sufi">Sufi</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="listen_songs" value="Devotional">Devotional</label>
											</div>
										
											<div class="wq_singleAnswerCtr wq_IsTrivia" data-crt="0" style="background-color:#f2f2f2; color:#444;">
												<label class="wq_answerTxtCtr"><input type="radio" name="listen_songs" value="Classical">Classical</label>
											</div>
										</div>
											</div>
											<div class="wq_triviaQuestionExplanation">
												<div class="wq_ExplanationHead"></div>
												<p class="wq_QuestionExplanationText"></p>
											</div>
										</div>
										<div class="wq_continue" style="display:none1;">
											<button class="wq_btn-continue" style="background-color:#00c479">Continue &gt;&gt;</button>
										</div>
									</div>
								
				   	</div>
					
					<input type="submit" name="submit_quiz" value="Submit Quiz">
					</form>	
				   	<div class="wq_resultsCtr">
				        
									<div style="display:none1;" class="wq_singleResultWrapper wq_IsTrivia" data-id="0" data-min="0" data-max="0">
										<span class="wq_quizTitle">test11</span>
										<div class="wq_resultScoreCtr"></div>
										<div class="wq_resultTitle"><strong>0 match</strong></div>
										
										<div class="wq_resultDesc">0 match</div>
										<!-- social share -->
				<div class="wq_shareCtr">
				<p style="font-size:14px;">Share your Results :</p>
				<button class="wq_shareFB"><i class="sprite sprite-facebook"></i><span>Facebook</span></button>
				<button class="wq_shareTwitter"><i class="sprite sprite-twitter"></i><span>Twitter</span></button>
				<button class="wq_shareGP"><i class="sprite sprite-google-plus"></i><span>Google+</span></button>
				</div>
				<!--// social share-->
									</div>
								
									<div style="display:none1;" class="wq_singleResultWrapper wq_IsTrivia" data-id="1" data-min="1" data-max="1">
										<span class="wq_quizTitle">test11</span>
										<div class="wq_resultScoreCtr"></div>
										<div class="wq_resultTitle"><strong>1 match Found</strong></div>
										
										<div class="wq_resultDesc">0 match</div>
										<!-- social share -->
				<div class="wq_shareCtr">
				<p style="font-size:14px;">Share your Results :</p>
				<button class="wq_shareFB"><i class="sprite sprite-facebook"></i><span>Facebook</span></button>
				<button class="wq_shareTwitter"><i class="sprite sprite-twitter"></i><span>Twitter</span></button>
				<button class="wq_shareGP"><i class="sprite sprite-google-plus"></i><span>Google+</span></button>
				</div>
				<!--// social share-->
									</div>
								
									<div style="display:none1;" class="wq_singleResultWrapper wq_IsTrivia" data-id="2" data-min="2" data-max="2">
										<span class="wq_quizTitle">test11</span>
										<div class="wq_resultScoreCtr"></div>
										<div class="wq_resultTitle"><strong>2 match found</strong></div>
										
										<div class="wq_resultDesc">2 match</div>
										<!-- social share -->
				<div class="wq_shareCtr">
				<p style="font-size:14px;">Share your Results :</p>
				<button class="wq_shareFB"><i class="sprite sprite-facebook"></i><span>Facebook</span></button>
				<button class="wq_shareTwitter"><i class="sprite sprite-twitter"></i><span>Twitter</span></button>
				<button class="wq_shareGP"><i class="sprite sprite-google-plus"></i><span>Google+</span></button>
				</div>
				<!--// social share-->
									</div>
								
									<div style="display:none1;" class="wq_singleResultWrapper wq_IsTrivia" data-id="3" data-min="3" data-max="3">
										<span class="wq_quizTitle">test11</span>
										<div class="wq_resultScoreCtr"></div>
										<div class="wq_resultTitle"><strong>3 match found</strong></div>
										
										<div class="wq_resultDesc">3</div>
										<!-- social share -->
				<div class="wq_shareCtr">
				<p style="font-size:14px;">Share your Results :</p>
				<button class="wq_shareFB"><i class="sprite sprite-facebook"></i><span>Facebook</span></button>
				<button class="wq_shareTwitter"><i class="sprite sprite-twitter"></i><span>Twitter</span></button>
				<button class="wq_shareGP"><i class="sprite sprite-google-plus"></i><span>Google+</span></button>
				</div>
				<!--// social share-->
									</div>
								
				   	</div>
				   	<!-- promote link -->
				        
				   	<!--// promote link-->
				   	<!-- retake button -->
				        <div class="wq_retakeQuizCtr">
				<button style="display:none1;" class="wq_retakeQuizBtn"><i class="fa fa-undo"></i>&nbsp;PLAY AGAIN !</button>
				</div>
				   	<!--// retake button-->
				</div>
									<?php	
									return ob_get_clean();  
									
							}
						

						/**
						 * Register Quiz post type
						 */
						public static function register_post_type() {

							$labels = array(
								'name'               => __( 'WP Quiz', 'wp-quiz' ),
								'menu_name'          => __( 'WP Quiz', 'wp-quiz' ),
								'singular_name'      => __( 'WP Quiz', 'wp-quiz' ),
								'name_admin_bar'     => _x( 'WP Quiz', 'name admin bar', 'wp-quiz' ),
								'all_items'          => __( 'All Quizzes', 'wp-quiz' ),
								'search_items'       => __( 'Search Quizzes', 'wp-quiz' ),
								'add_new'            => _x( 'Add New', 'quiz', 'wp-quiz' ),
								'add_new_item'       => __( 'Add New WP Quiz', 'wp-quiz' ),
								'new_item'           => __( 'New Quiz', 'wp-quiz' ),
								'view_item'          => __( 'View Quiz', 'wp-quiz' ),
								'edit_item'          => __( 'Edit Quiz', 'wp-quiz' ),
								'not_found'          => __( 'No Quizzes Found.', 'wp-quiz' ),
								'not_found_in_trash' => __( 'WP Quiz not found in Trash.', 'wp-quiz' ),
								'parent_item_colon'  => __( 'Parent Quiz', 'wp-quiz' ),
							);

							$args = array(
								'labels'             => $labels,
								'description'        => __( 'Holds the quizzes and their data.', 'wp-quiz' ),
								'menu_position'      => 5,
								'menu_icon'          => 'dashicons-editor-help',
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'query_var'          => true,
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title', 'author', 'thumbnail', 'excerpt' ),
							);

							register_post_type( 'wp_quiz', $args );
						}

						/**
						 * Shortcode used to display quiz
						 *
						 * @return string HTML output of the shortcode
						 */
						public function register_shortcode( $atts ) {

							if ( ! isset( $atts['id'] ) ) {
								return false;
							}

							// we have an ID to work with
							$quiz = get_post( $atts['id'] );

							// check if ID is correct
							if ( ! $quiz || 'wp_quiz' !== $quiz->post_type ) {
								return "<!-- wp_quiz {$atts['id']} not found -->";
							}

							// lets go
							$this->set_quiz( $atts['id'] );
							$this->quiz->enqueue_scripts();

							return $this->quiz->render_public_quiz();
						}

						/**
						 * Set the current quiz
						 */
						public function set_quiz( $id ) {

							$quiz_type  = get_post_meta( $id, 'quiz_type', true );
							$quiz_type  = str_replace( '_quiz', '', $quiz_type );
							$quiz_type  = 'WP_Quiz_' . ucwords( $quiz_type ) . '_Quiz';
							$this->quiz = new $quiz_type( $id );
						}

						/**
						 * [create_quiz_page description]
						 * @param  [type] $content [description]
						 * @return [type]          [description]
						 */
						public function create_quiz_page( $content ) {

							global $post;

							if ( 'wp_quiz' !== $post->post_type ) {
								return $content;
							}

							if ( ! is_single() ) {
								return $content;
							}

							$quiz_html = $this->register_shortcode( array( 'id' => $post->ID ) );

							return $quiz_html . $content;
						}

						public function check_image_file() {

							$output = array( 'status' => 1 );
							$check  = false;
							if ( @getimagesize( $_POST['url'] ) ) {
								$check = true;
							}

							$output['check'] = $check;
							wp_send_json( $output );
						}

						public function check_video_file() {

							$output  = array( 'status' => 1 );
							$check   = false;
							$id      = $_POST['video_id'];
							$url     = "//www.youtube.com/oembed?url=http://www.youtube.com/watch?v=$id&format=json";
							$headers = get_headers( $url );

							if ( '404' !== substr( $headers[0], 9, 3 ) ) {
								$check = true;
							}

							$output['check'] = $check;
							wp_send_json( $output );
						}

						public function get_debug_log() {
							$page = new WP_Quiz_Page_Support();
							$page->get_debug_log();
						}

						public static function activate_plugin() {

							// Don't activate on anything less than PHP 5.4.0 or WordPress 3.4
							if ( version_compare( PHP_VERSION, '5.4.0', '<' ) || version_compare( get_bloginfo( 'version' ), '3.4', '<' ) || ! function_exists( 'spl_autoload_register' ) ) {
								require_once ABSPATH . 'wp-admin/includes/plugin.php';
								deactivate_plugins( basename( __FILE__ ) );
								wp_die( __( 'WP Quiz requires PHP version 5.4.0 with spl extension or greater and WordPress 3.4 or greater.', 'wp-quiz' ) );
							}

							// Dont't activate if wp quiz pro is active
							if ( defined( 'WP_QUIZ_PRO_VERSION' ) ) {
								deactivate_plugins( basename( __FILE__ ) );
								wp_die( __( 'Please deactivate WP Quiz Pro plugin', 'wp-quiz' ) );
							}

							include( 'inc/activate-plugin.php' );
						}

						public function fb_share_fix() {

							$data   = array_map( 'urldecode', $_GET );
							$result = get_post_meta( $data['id'], 'results', true );
							$result = isset( $result[ $data['rid'] ] ) ? $result[ $data['rid'] ] : array();

							// Picture
							if ( 'r' === $data['pic'] ) {
								$data['source'] = $result['image'];
							} elseif ( 'f' === $data['pic'] ) {
								$data['source'] = wp_get_attachment_url( get_post_thumbnail_id( $data['id'] ) );
							} elseif ( ( substr( $data['pic'], 0, 6 ) === 'image-' ) ) {
								$upload_dir            = wp_upload_dir();
								$upload_dir['baseurl'] = $upload_dir['baseurl'] . '/wp_quiz-result-images';
								$data['source']        = $upload_dir['baseurl'] . '/' . $data['pic'] . '.png';
							} else {
								$data['source'] = false;
							}

							// Description
							if ( 'r' === $data['desc'] ) {
								$data['description'] = $result['desc'];
							} elseif ( 'e' === $data['desc'] ) {
								$data['description'] = get_post_field( 'post_excerpt', $data['id'] );
							} else {
								$data['description'] = false;
							}

							$settings = get_option( 'wp_quiz_default_settings' );
							$url      = ( is_ssl() ? 'https' : 'http' ) . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

							global $post;
							$pid          = $post ? $post->ID : $data['id'];
							$original_url = get_permalink( $pid );
							?>
							<html>
								<head>
									<title><?php wp_title( '' ); ?></title>
									<meta property="fb:app_id" content="<?php echo $settings['defaults']['fb_app_id']; ?>">
									<meta property="og:type" content="website">
									<meta property="og:url" content="<?php echo esc_url( $url ); ?>">
									<meta name="twitter:card" content="summary_large_image">
									<?php
									if ( ! empty( $data['text'] ) ) :
										$title = get_the_title( $pid );
										$text  = esc_attr( $data['text'] );

										$title = $title === $text ? $title : $title . ' - ' . $text;
									?>
									<meta property="og:title" content="<?php echo $title; ?>">
									<meta property="twitter:title" content="<?php echo $title; ?>">
									<?php endif; ?>
									<?php if ( ! empty( $data['source'] ) ) : ?>
									<meta property="og:image" content="<?php echo esc_url( $data['source'] ); ?>">
									<meta property="twitter:image" content="<?php echo esc_url( $data['source'] ); ?>">
										<?php list( $img_width, $img_height ) = getimagesize( $data['source'] ); ?>
										<?php if ( isset( $img_width ) && $img_width ) : ?>
										<meta property="og:image:width" content="<?php echo $img_width; ?>">
										<?php endif; ?>
										<?php if ( isset( $img_height ) && $img_height ) : ?>
										<meta property="og:image:height" content="<?php echo $img_height; ?>">
										<?php endif; ?>
									<?php endif; ?>
									<?php if ( ! empty( $data['description'] ) ) : ?>
									<meta property="og:description" content="<?php echo esc_attr( $data['description'] ); ?>">
									<meta property="twitter:description" content="<?php echo esc_attr( $data['description'] ); ?>">
									<?php endif; ?>
									<meta http-equiv="refresh" content="0;url=<?php echo esc_url( $original_url ); ?>">
								</head>
							<body>
								Redirecting please wait....
							</body>
							</html>
							<?php
							exit;
						}

						public function inline_script() {
							$settings = get_option( 'wp_quiz_default_settings' );
							?>
								<script>
								<?php if ( ! empty( $settings['defaults']['fb_app_id'] ) ) { ?>
									window.fbAsyncInit = function() {
										FB.init({
											appId    : '<?php echo $settings['defaults']['fb_app_id']; ?>',
											xfbml    : true,
											version  : 'v2.9'
										});
									};

									(function(d, s, id){
										var js, fjs = d.getElementsByTagName(s)[0];
										if (d.getElementById(id)) {return;}
										js = d.createElement(s); js.id = id;
										js.src = "//connect.facebook.net/en_US/sdk.js";
										fjs.parentNode.insertBefore(js, fjs);
									}(document, 'script', 'facebook-jssdk'));
								<?php } ?>
								</script>
							<?php
							if ( is_singular( array( 'wp_quiz' ) ) && isset( $settings['defaults']['share_meta'] ) && 1 === $settings['defaults']['share_meta'] ) {
								global $post, $wpseo_og;
								$twitter_desc = $og_desc = str_replace( array( "\r", "\n" ), '', strip_tags( $post->post_excerpt ) );
								if ( defined( 'WPSEO_VERSION' ) ) {
									remove_action( 'wpseo_head', array( $wpseo_og, 'opengraph' ), 30 );
									remove_action( 'wpseo_head', array( 'WPSEO_Twitter', 'get_instance' ), 40 );
									//use description from yoast
									$twitter_desc = get_post_meta( $post->ID, '_yoast_wpseo_twitter-description', true );
									$og_desc      = get_post_meta( $post->ID, '_yoast_wpseo_opengraph-description', true );
								}
								?>
								<meta name="twitter:title" content="<?php echo get_the_title(); ?>">
								<meta name="twitter:description" content="<?php echo $twitter_desc; ?>">
								<meta name="twitter:domain" content="<?php echo esc_url( site_url() ); ?>">
								<meta property="og:url" content="<?php the_permalink(); ?>" />
								<meta property="og:title" content="<?php echo get_the_title(); ?>" />
								<meta property="og:description" content="<?php echo $og_desc; ?>" />
								<?php
								if ( has_post_thumbnail() ) {
									$thumb_id        = get_post_thumbnail_id();
									$thumb_url_array = wp_get_attachment_image_src( $thumb_id, 'full', true );
									$thumb_url       = $thumb_url_array[0];
									?>
									<meta name="twitter:card" content="summary_large_image">
									<meta name="twitter:image:src" content="<?php echo $thumb_url; ?>">
									<meta property="og:image" content="<?php echo $thumb_url; ?>" />
									<meta itemprop="image" content="<?php echo $thumb_url; ?>">
								<?php
								}
							}
						}

						/**
						 * Get plugin directory.
						 * @return string
						 */
						public function plugin_dir() {
							if ( is_null( $this->plugin_dir ) ) {
								$this->plugin_dir = untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/';
							}
							return $this->plugin_dir;
						}

						/**
						 * Get plugin uri.
						 * @return string
						 */
						public function plugin_url() {
							if ( is_null( $this->plugin_url ) ) {
								$this->plugin_url = untrailingslashit( plugin_dir_url( __FILE__ ) ) . '/';
							}
							return $this->plugin_url;
						}
					}

					/**
					 * Main instance of WP_Quiz_Plugin.
					 *
					 * Returns the main instance of WP_Quiz_Plugin to prevent the need to use globals.
					 *
					 * @return WP_Quiz_Plugin
					 */

					function wp_quiz() {
						return WP_Quiz_Plugin::get_instance();
					}

				endif;

				add_action( 'plugins_loaded', 'wp_quiz', 10 );
				register_activation_hook( __FILE__, array( 'WP_Quiz_Plugin', 'activate_plugin' ) );
